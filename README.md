# ClassicMayan Tools – Queries Collection

## Queries to get data from several endpoints

* SPARQL-Endpoint (<https://www.classicmayan.org/trip/dataset.html?ds=/metadata>)
* TextGrid-Search
* ConedaKor (<https://classicmayan.kor.de.dariah.eu/entities.json>)


## Save data to TextGrid Repository

Additionally it provides function to save data into the TextGrid-Repository to get TextGrid URIs for referencing the images from RDF form data.

### Harvesting from ConedaKOR to TextGrid

#### Ziel

Publizierfähige (Bild)-Daten und Metadaten werden von ConedaKor in das TextGrid Repository (OwnStorage) eingespielt, um dann per TextGrid OAI-PMH (MetadataFormat **oai_idiom_mets** aus Projekt „idiom“ <https://dev.textgridlab.org/1.0/tgoaipmh/oai?verb=ListRecords&metadataPrefix=oai_idiom_mets>) in die Visual Library der Uni Bonn übernehmen zu können.

Link zur VL: <https://digitale-sammlungen.ulb.uni-bonn.de>


#### Workflow

* Bilder mit Metadaten werden in einer ConedaKOR-Instanz gehostet und publiziert <https://classicmayan.kor.de.dariah.eu>.
* Bilder und Metadaten sind dort mit Zeitstempel versehen, d.h. wann erstellt, wann geändert, wann gelöscht.
* **Nur** die Metadaten der Bilder werden von KOR nach TextGrid via KOR API geharvestet, hier wird geprüft, ob etwas neu ist (neues Bild, neuer Metadateneintrag) oder geändert bzw. gelöscht wurde (Bild geändert, bzw. Metadaten geändert, gelöscht). In den Metadaten werden dann die Bild-Links mitgeschickt, die von der Visual Library Harvest benutzt werden, die Bilder in KOR abzuholen.
* Über die KOR API werden die Metadaten nun abgefragt: Harvesten, wenn etwas neu (neues Bild, neue Metadaten) ist bzw. wenn bestehende Einträge geändert wurden.
* Die Metadaten der Bilder werden dann in TextGrid importiert (non-public), damit die Bilder mit ihrer TextGrid URI aus den RDF-Formulardaten referenziert werden können.
* Gespeichert wird in der Kollektion „ConedaKORMediumMetadata“ im Projekt „idiom“, bisher 6 Unterkollektionen 1-6 mit jeweils 2500 Datensätzen.
* Fragen
    * Wie verhält es sich, wenn Daten aktualisiert werden? Werden diese überschrieben oder blieben sie bestehen? --> Die Daten werden überschrieben (TextGrid#UPDATE), da es sich (momentan noch) nicht um publizierte Daten handelt.
    * Wie werden in ConedaKOR gelöschte Daten behandelt/ermittelt? In ConedaKOR gelöschte Datensätze müssen bisher händisch aus TextGrid gelöscht werden.

#### How to do it

New ready-to-go images from ConedaKor shall be published in the TextGrid Repository from time to time triggered by the researchers from the project. Here's how to do this:

1. Determine the latest object in project “idiom“ (TGPR-0e926f53-1aba-d415-ecf6-539edcd8a318), collection “ConedaKorMediumMetadata“: Look for the latest subcollection entry in the Navigator, “6“ at the moment, and take the date from “Last change“ or “<generic><generated><lastModified>“ value from the subcollection, please right click on the entry to “Show technical metadata“ to get the needed date.
2. Determine which data already has been imported to TG and was **updated** in ConedaKOR (for **updating** the objects in TG), **AND** which data has been **added** to ConedaKOR (for **creating** new objects)!
    * Get all objects from ConedaKOR created **before THE DATE** (e.g. 2022-03-18), and modified **after THE DATE**: <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&created_before=2022-03-18&updated_after=2022-03-18&include=technical,dataset>
    * Get all newly created objects from ConedaKOR after **THE DATE** of latest import (e.g. 2022-03-18): <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&created_after=2022-03-18&include=technical,dataset>
3. Import or update all the new image metadata using the class **org.classicmayan.tools.ConedaKor2TextGridRep.java**
    * Check the properties file “idiomQueries.properties“ and create your own copy of it (**!DO NOT PUSH YOUR COPY INTO THE GIT REPOSITORY!**, for there are secret secrets involved!)
    * Set your chosen config filename in class **org.classicmayan.tools.IdiomConstants.java**.
    * Fill in all the needed information that is documented in the config file itself. Three scopes are available, “update“, “importNew“, or “importAll“. Please use **update** and **importNew** for updating the TextGridRep with all modified and new ConedaKOR data!
        * **update** updates all the files modified since the above date (gets 500 objects at a time, please repeat if you want to update more),
        * **importNew** only imports all new files since the aboce date (imports 500 new objects at a time, please repeat if you want to import more), and
        * **importAll** imports all the applicable data from ConedaCOR in a new collection with name “ConedaKorMediumMetadata“ (not really needed at the moment), including all the subcollections to be created (importa all the items, no need for repeating anything).
        * **importMissing** looks up every ConedaKOR ID in TextGrid titles and/or notes tags, and imports every missing metadata to TextGrid. Use **dryRun** to check only!
        * **updateAndImportNew** does **update** and **importNew** one after the other. Use **dryRun** to check only!
    * Try starting the main class using **dryRun=true** to see, what would happen, start main class using **dryRun=false** to make it happen!
    * Test for newly imported objects in OAI-PMH (please set **from** and **to** values correctly): <https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListRecords&metadataPrefix=oai_idiom_mets&from=2022-12-01&until=2022-12-06>

### Useful queries

#### TG-oaipmh

Check for newly imported/updated items in TextGrid OAI-PMH (please set timestamps accordingly):

* <https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListRecords&metadataPrefix=oai_idiom_mets&from=2023-02-10T00:00:00&until=2023-02-10T16:21:00>


#### TG-search

* Documentation: <https://textgridlab.org/doc/services/submodules/tg-search/docs/>
* All children of “ConedaKorMediumMetadata“ collection (recursively): <https://textgridlab.org/1.0/tgsearch/info/textgrid:3x63n.0/children>
* All aggregations of  “ConedaKorMediumMetadata“ collection: <https://textgridlab.org/1.0/tgsearch/navigation/agg/textgrid:3x63n.0>
* All children of “5“ collection: <https://textgridlab.org/1.0/tgsearch/info/textgrid:3x63m.0/children>
* All children of “6“ collection: <https://textgridlab.org/1.0/tgsearch/info/textgrid:3xpzt.0/children>
* Latest object's metadata: <https://textgridlab.org/1.0/tgsearch/info/textgrid:3xsn8.0/metadata?sid=SID>
* Search for KOR IDs in TG metadata titles: <https://textgridlab.org/1.0/tgsearch/search?q=title:27361&sid=SID>
* Search for KOR IDs in TG metadata notes: <https://textgridlab.org/1.0/tgsearch/search?q=notes:27361&sid=SID>


#### ConedaKOR queries

* OAI-PMH
    * <https://classicmayan.kor.de.dariah.eu/oai-pmh/entities?verb=ListIdentifiers>
* REST
    * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&include=technical,dataset>
    * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&created_after=2022-05-01&include=technical,dataset>
    * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&updated_after=2022-05-01&include=technical,dataset>
    * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&created_after=&created_before=&per_page=&page=&include=technical,dataset>
    * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&created_before=2022-03-18&updated_after=2022-03-18&include=technical,dataset>
    * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&id=27662&include=technical,dataset>

## Releasing a new version

For releasing a new version of Queries Collection, please have a look at the [DARIAH-DE Release Management Page](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) or see the [Gitlab CI file](.gitlab-ci.yml).
