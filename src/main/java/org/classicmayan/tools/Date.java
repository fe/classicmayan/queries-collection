package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Date {

  private List<String> beginLongcount = new ArrayList<String>();
  private List<String> endLongcount = new ArrayList<String>();
  private List<String> longcountTimeSpan = new ArrayList<String>();
  private List<String> gregorianTimeSpan = new ArrayList<String>();
  private List<String> calenderRoundTimeSpan = new ArrayList<String>();
  private List<String> beginCalenderRound = new ArrayList<String>();
  private List<String> endCalenderRound = new ArrayList<String>();
  private List<String> beginGregorianDate = new ArrayList<String>();
  private List<String> endGregorianDate = new ArrayList<String>();

  /**
   * @param longcount
   */
  public void addBeginLongcount(String longcount) {
    this.beginLongcount.add(longcount);
  }

  /**
   * @return
   */
  public List<String> getCalenderRoundTimeSpan() {
    return this.calenderRoundTimeSpan;
  }

  /**
   * @param calenderRoundTimeSpan
   */
  public void setCalenderRoundTimeSpan(List<String> calenderRoundTimeSpan) {
    this.calenderRoundTimeSpan = calenderRoundTimeSpan;
  }

  /**
   * @return
   */
  public List<String> getGregorianTimeSpan() {
    return this.gregorianTimeSpan;
  }

  /**
   * @param gregorianTimeSpan
   */
  public void setGregorianTimeSpan(List<String> gregorianTimeSpan) {
    this.gregorianTimeSpan = gregorianTimeSpan;
  }

  /**
   * @param longcount
   */
  public void addEndLongcount(String longcount) {
    this.endLongcount.add(longcount);
  }

  /**
   * @param longcountTimeSpan
   */
  public void addLongcountTimeSpan(String longcountTimeSpan) {
    this.longcountTimeSpan.add(longcountTimeSpan);
  }

  /**
   * @param gregorianTimeSpan
   */
  public void addGregorianTimeSpan(String gregorianTimeSpan) {
    this.gregorianTimeSpan.add(gregorianTimeSpan);
  }

  /**
   * @param calenderRound
   */
  public void addCalenderRoundTimeSpan(String calenderRound) {
    this.calenderRoundTimeSpan.add(calenderRound);
  }

  /**
   * @param calenderRound
   */
  public void addBeginCalenderRound(String calenderRound) {
    this.beginCalenderRound.add(calenderRound);
  }

  /**
   * @param calenderRound
   */
  public void addEndCalenderRound(String calenderRound) {
    this.endCalenderRound.add(calenderRound);
  }

  /**
   * @param gregorianDate
   */
  public void addBeginnGregorianDate(String gregorianDate) {
    this.beginGregorianDate.add(gregorianDate);
  }

  /**
   * @param gregorianDate
   */
  public void addEndGregorianDate(String gregorianDate) {
    this.endGregorianDate.add(gregorianDate);
  }

  /**
   * @return
   */
  public List<String> getLongcountTimeSpan() {
    return this.longcountTimeSpan;
  }

  /**
   * @param longcountTimeSpan
   */
  public void setLongcountTimeSpan(List<String> longcountTimeSpan) {
    this.longcountTimeSpan = longcountTimeSpan;
  }

  /**
   * @return
   */
  public List<String> getEndGregorianDate() {
    return this.endGregorianDate;
  }

  /**
   * @param endGregorianDate
   */
  public void setEndGregorianDate(List<String> endGregorianDate) {
    this.endGregorianDate = endGregorianDate;
  }

  /**
   * @return
   */
  public List<String> getBeginGregorianDate() {
    return this.beginGregorianDate;
  }

  /**
   * @param beginGregorianDate
   */
  public void setBeginGregorianDate(List<String> beginGregorianDate) {
    this.beginGregorianDate = beginGregorianDate;
  }

  /**
   * @return
   */
  public List<String> getEndCalenderRound() {
    return this.endCalenderRound;
  }

  /**
   * @param endCalenderRound
   */
  public void setEndCalenderRound(List<String> endCalenderRound) {
    this.endCalenderRound = endCalenderRound;
  }

  /**
   * @return
   */
  public List<String> getBeginCalenderRound() {
    return this.beginCalenderRound;
  }

  /**
   * @param beginCalenderRound
   */
  public void setBeginCalenderRound(List<String> beginCalenderRound) {
    this.beginCalenderRound = beginCalenderRound;
  }

  /**
   * @return
   */
  public List<String> getEndLongcount() {
    return this.endLongcount;
  }

  /**
   * @param endLongcount
   */
  public void setEndLongcount(List<String> endLongcount) {
    this.endLongcount = endLongcount;
  }

  /**
   * @return
   */
  public List<String> getBeginLongcount() {
    return this.beginLongcount;
  }

  /**
   * @param beginLongcount
   */
  public void setBeginLongcount(List<String> beginLongcount) {
    this.beginLongcount = beginLongcount;
  }

}
