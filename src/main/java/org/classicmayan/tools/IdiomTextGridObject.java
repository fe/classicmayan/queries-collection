/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.imageio.ImageIO;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.bind.JAXB;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.cxf.helpers.IOUtils;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import info.textgrid.clients.CrudClient;
import info.textgrid.clients.tgcrud.CreateOperation;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.clients.tgcrud.TextGridObject;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;

/**
 *
 */
public class IdiomTextGridObject {

  // **
  // FINALS
  // **

  private static final String METADATA_TEMPLATE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<ns3:tgObjectMetadata\n" +
      "xmlns:ns3=\"http://textgrid.info/namespaces/metadata/core/2010\"\n" +
      "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
      "xsi:schemaLocation=\"http://textgrid.info/namespaces/metadata/core/2010 http://textgridlab.org/schema/textgrid-metadata_2010.xsd\">\n"
      +
      "<ns3:object>\n" +
      "<ns3:generic>\n" +
      "<ns3:provided>\n" +
      "<ns3:title></ns3:title>\n" +
      "<ns3:format></ns3:format>\n" +
      "</ns3:provided>\n" +
      "</ns3:generic>\n" +
      "<ns3:item />\n" +
      "</ns3:object>\n" +
      "</ns3:tgObjectMetadata>\n";
  private static final String COLLECTION_TEMPLATE_START =
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
          + "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
          + "    xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
          + "    <rdf:Description xmlns:tei=\"http://www.tei-c.org/ns/1.0\" rdf:about=\"\">\n";
  private static final String COLLECTION_TEMPLATE_END =
      "    </rdf:Description>\n"
          + "</rdf:RDF>\n";

  // **
  // STATIC
  // **

  private static CrudClient crudClient;
  protected static Logger log = Logger.getLogger(IdiomTextGridObject.class.getName());


  // **
  // CLASS
  // **

  private String textgridURI;
  private String textgridURL;
  private String scope;
  private boolean dryRun = true;
  private String textgridCrudEndpoint;
  private String sessionID;
  private String projectID;
  private String formatToFilter;
  private String date;
  private String querySize;
  private String conedaKorMediumMetadataCollectionURI;
  private int maxConedaKorCollectionSize = 2500;

  /**
   * @param propertiesFile
   * @throws IOException
   * @throws CrudClientException
   */
  public IdiomTextGridObject(String propertiesFile) throws IOException, CrudClientException {

    String props;
    if (propertiesFile != null && !propertiesFile.isEmpty()) {
      props = propertiesFile;
    } else {
      props = IdiomConstants.PROPERTIES_FILE;
    }

    try (InputStream input = new FileInputStream(props)) {
      Properties p = new Properties();
      p.load(input);

      this.dryRun = Boolean.parseBoolean(p.getProperty(IdiomConstants.DRY_RUN_PROPERTY));
      this.scope = p.getProperty(IdiomConstants.SCOPE_PROPERTY);
      this.textgridURL = p.getProperty(IdiomConstants.TGURL_PROPERTY);
      if (this.textgridURL == null || this.textgridURL.equals("")) {
        this.textgridURL = IdiomConstants.DEFAULT_TGURL;
      }
      this.sessionID = p.getProperty(IdiomConstants.SID_PROPERTY);
      this.projectID = p.getProperty(IdiomConstants.PID_PROPERTY);
      this.formatToFilter = p.getProperty(IdiomConstants.FORMAT_TO_FILTER_PROPERTY);
      this.querySize = p.getProperty(IdiomConstants.QUERY_SIZE_PROPERTY);
      this.textgridCrudEndpoint = p.getProperty(IdiomConstants.TGCRUD_ENDPOINT_PROPERTY);
      if (this.textgridCrudEndpoint == null || this.textgridCrudEndpoint.equals("")) {
        this.textgridCrudEndpoint = CrudClient.DEFAULT_ENDPOINT;
      }
      this.conedaKorMediumMetadataCollectionURI =
          p.getProperty(IdiomConstants.MEDIUM_COLLECTION_PROPERTY);
      this.date = p.getProperty(IdiomConstants.DATE_PROPERTY);
      this.maxConedaKorCollectionSize =
          Integer.parseInt(p.getProperty(IdiomConstants.MAX_COLLECTION_SIZE_PROPERTY));

      input.close();

      // Then create singleton TG-crud client.
      if (crudClient == null) {
        crudClient = new CrudClient(this.textgridCrudEndpoint).enableGzipCompression();
      }
    }
  }

  /**
   * @param textGridURI
   * @return
   * @throws CrudClientException
   * @throws IOException
   */
  public String getDataAsString(String textGridURI) throws CrudClientException, IOException {

    TextGridObject tgobj = IdiomTextGridObject.crudClient.read()
        .setSid(this.sessionID)
        .setTextgridUri(textGridURI)
        .execute();

    return IOUtils.readStringFromStream(tgobj.getData());
  }

  /**
   * @param textGridURI
   * @param sid
   * @param size
   * @return
   * @throws IOException
   */
  public BufferedImage getTextGridObjectByUri(String textGridURI, String sid, int size)
      throws IOException {

    // FIXME Why not get the object directly from TG-crud?

    BufferedImage image = ImageIO.read(new URL(this.textgridURL + "digilib/rest/IIIF/"
        + textGridURI + ";sid=" + sid + "/full/," + size + "/0/native.jpg"));
    if (image.getHeight() < image.getWidth()) {
      return ImageIO.read(new URL(this.textgridURL + "digilib/rest/IIIF/" + textGridURI + ";sid="
          + sid + "/full/" + size + ",/0/native.jpg"));
    } else {
      return ImageIO.read(new URL(this.textgridURL + "digilib/rest/IIIF/" + textGridURI + ";sid="
          + sid + "/full/," + size + "/0/native.jpg"));
    }
  }

  /**
   * @throws CrudClientException
   */
  public void consistencyCheckArtefact() {

    List<String> artefacts = TripleStoreQuery.getArtefactList();

    for (String artefact : artefacts) {
      try {
        IdiomTextGridObject.crudClient.read()
            .setSid(this.sessionID)
            .setTextgridUri(artefact.replace("http://textgridrep.de/", "textgrid:"))
            .execute();
      } catch (CrudClientException e) {
        log.severe(artefact.replace("http://textgridrep.de/", "textgrid:"));
      }
    }
  }

  /**
   * @param textGridURI
   * @return
   * @throws CrudClientException
   * @throws SAXException
   * @throws IOException
   * @throws JDOMException
   * @throws ParserConfigurationException
   */
  public Document getDataAsXMLObject(String textGridURI) throws CrudClientException, SAXException,
      IOException, JDOMException, ParserConfigurationException {

    info.textgrid.clients.tgcrud.TextGridObject tgobj = IdiomTextGridObject.crudClient.read()
        .setSid(this.sessionID)
        .setTextgridUri(textGridURI)
        .execute();
    SAXBuilder builderJDOM = new SAXBuilder();
    Document doc = (Document) builderJDOM
        .build(new InputSource(new StringReader(IOUtils.readStringFromStream(tgobj.getData()))));

    return doc;
  }

  /**
   * @param jsonFile
   * @param title
   * @param aggregationURI
   * @param parserFile
   * @throws IOException
   * @throws CrudClientException
   */
  public void saveToTextGrid(JSONObject json, String title, boolean parserFile)
      throws IOException, CrudClientException {

    MetadataContainerType metadata = createMetadata(title, "application/json");

    // JAXB.marshal(metadata, System.out);
    String jsonData = json.toString(2);
    DataHandler objectData =
        new DataHandler(new ByteArrayDataSource(jsonData.getBytes(), "application/octet-stream"));
    String mediumFileID = json.get("id").toString();
    TextGridObject tgObject = new TextGridObject();
    if (parserFile) {
      metadata.getObject().getGeneric().getProvided()
          .setNotes("ConedaKorMediumData " + mediumFileID);
      tgObject.setMetadatada(metadata);
      tgObject.setData(objectData.getInputStream());
    }

    // JAXB.marshal(metadata, System.out);

    // TODO See <https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients/-/issues/2>.
    if (!this.dryRun) {
      CreateOperation create = IdiomTextGridObject.crudClient.create();
      String tguri = create.setSid(this.sessionID)
          .setProjectId(this.projectID)
          .setObject(tgObject)
          .execute();
      this.textgridURI = tguri;

      log.fine(this.textgridURI + " [JSON CREATED]");

    } else {
      this.generateFakeTextGridURI();

      log.fine(this.textgridURI + " [DRYRUN]");
    }
  }

  /**
   * @param json
   * @param title
   * @throws IOException
   * @throws CrudClientException
   */
  public void updateToTextGrid(JSONObject json, String title)
      throws IOException, CrudClientException {

    // Get existing metadata from TG-crud.
    MetadataContainerType metadata = IdiomTextGridObject.crudClient.readMetadata()
        .setSid(this.sessionID).setTextgridUri(this.textgridURI).execute();

    // Update title, if KOR ID in title is the same as in JSON metadata.
    // TODO Check for more titles?
    String oldTitle = metadata.getObject().getGeneric().getProvided().getTitle().get(0);
    String korID = String.valueOf(json.getInt("id"));
    if (oldTitle.startsWith("[" + korID + "]")) {
      metadata.getObject().getGeneric().getProvided().getTitle().remove(0);
      metadata.getObject().getGeneric().getProvided().getTitle().add(title);
    } else {
      log.severe("TG Object title: '" + oldTitle + "'");
      log.severe("New KOR title:   '" + title + "'");
      throw new IOException("Old TG object title does not fit KOR title!");
    }

    String jsonData = json.toString(2);
    DataHandler objectData =
        new DataHandler(new ByteArrayDataSource(jsonData.getBytes(), "application/octet-stream"));
    TextGridObject tgObject = new TextGridObject();
    tgObject.setMetadatada(metadata);
    tgObject.setData(objectData.getInputStream());

    if (!this.dryRun) {
      IdiomTextGridObject.crudClient.update()
          .setSid(this.sessionID)
          .setObject(tgObject)
          .execute();

      log.fine(" [UPDATED]");
    } else {
      log.fine(" [DRYRUN]");
    }

  }

  /**
   * <p>
   * Add a number of URIs to an existing collection.
   * </p>
   * 
   * @param objectURIs
   * @param collectionURI
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws CrudClientException
   */
  public void addUriListToAggregation(List<String> objectURIs, String collectionURI)
      throws CrudClientException, IOException {

    log.info("\tAdding " + objectURIs.size() + " entries to existing collection " + collectionURI);
    log.info("\tURI list (size: " + objectURIs.size() + "): " + objectURIs);

    TextGridObject tgobj = IdiomTextGridObject.crudClient.read()
        .setSid(this.sessionID)
        .setTextgridUri(collectionURI)
        .execute();
    String aggregation = IOUtils.readStringFromStream(tgobj.getData());

    // Check amount of entries.
    int entriesBefore = getResourceCount(aggregation);

    for (String uri : objectURIs) {
      aggregation = aggregation.replace("</rdf:Description>",
          "        <ore:aggregates rdf:resource=\""
              + uri + "\"/>\n"
              + "    </rdf:Description>");
    }

    // Check amount of entries.
    int entriesAfter = getResourceCount(aggregation);
    if (entriesAfter != entriesBefore + objectURIs.size()) {
      log.severe("!ERROR! WRONG ENTRIES COUNT IN COLLECTION " + collectionURI);
      log.severe("!ERROR! " + entriesAfter + " != " + entriesBefore + " + " + objectURIs.size());
    }

    tgobj.setData(new ByteArrayInputStream(aggregation.getBytes()));

    if (!this.dryRun) {
      IdiomTextGridObject.crudClient.update()
          .setSid(this.sessionID)
          .setObject(tgobj)
          .execute();
    }
  }

  /**
   * <p>
   * Updates the Metadata
   * </p>
   * 
   * FIXME ReadMetadata and UpdateMetadata would suffice here! TODO Implement reading and updating
   * metadata in textgrid-java-clients?
   * 
   * @throws CrudClientException
   */
  public void updateObject() throws CrudClientException {

    List<String> artefacts = TripleStoreQuery.getArtefactList();

    int counterForObjectsToProceeds = artefacts.size();
    // artefacts.add("textgrid:3q17c");
    for (String uri : artefacts) {

      TextGridObject tgObject = IdiomTextGridObject.crudClient.read()
          .setSid(this.sessionID)
          .setTextgridUri(uri.replace("http://textgridrep.de/", "textgrid:"))
          .execute();

      // Add item format as note and update Textgrid object.
      MetadataContainerType metadata = tgObject.getMetadatada();
      metadata.getObject().getGeneric().getProvided().setNotes("ARTEFACT");
      tgObject.setMetadatada(metadata);

      // Change uri here for what reason?
      uri = uri.replace(".0", "");

      // Update.
      IdiomTextGridObject.crudClient.update()
          .setSid(this.sessionID)
          .setObject(tgObject)
          .execute();

      counterForObjectsToProceeds--;

      log.info(counterForObjectsToProceeds + " objects left");
    }
  }

  /**
   * <p>
   * Create a new object in TextGrid.
   * </p>
   * 
   * @param mediumMetadata
   * @return
   * @throws IOException
   * @throws CrudClientException
   */
  public void transferConedaKorMediumToTextGrid(JSONObject mediumMetadata)
      throws IOException, CrudClientException {

    // Get ID and image description.
    String korID = mediumMetadata.get("id").toString();
    String description;
    try {
      description = mediumMetadata.getJSONObject("dataset").getString("image_description");
    } catch (JSONException e) {
      description = "**NO DESCRIPTION YET**";
    }

    // Get title from ID and JSON.
    String title = "[" + korID + "] " + description;

    // Prepare saving to TextGrid.
    this.saveToTextGrid(mediumMetadata, title, true);
  }

  /**
   * <p>
   * Update an existing object in TextGrid.
   * </p>
   * 
   * @param textGridURI
   * @param mediumMetadata
   * @return
   * @throws IOException
   * @throws CrudClientException
   */
  public void updateConedaKorMediumToTextGrid(JSONObject mediumMetadata)
      throws IOException, CrudClientException {

    // Get ID and image description.
    String korID = mediumMetadata.get("id").toString();
    String description;
    try {
      description = mediumMetadata.getJSONObject("dataset").getString("image_description");
    } catch (JSONException e) {
      description = "**NO DESCRIPTION YET**";
    }

    // Get title from ID and JSON.
    String title = "[" + korID + "] " + description;

    log.fine(this.textgridURI + ": " + title);

    // Prepare saving to TextGrid.
    this.updateToTextGrid(mediumMetadata, title);
  }

  /**
   * <p>
   * Creates a collection from the given data.
   * </p>
   * 
   * @param uris
   * @param title
   * @throws IOException
   * @throws CrudClientException
   */
  public void createCollection(List<String> uris, String title)
      throws IOException, CrudClientException {

    log.info("\tAdding " + uris.size() + " entries to new subcollection '" + title + "'");
    log.info("\tURI list (size: " + uris.size() + "): " + uris);

    MetadataContainerType metadata = createMetadata(title, TextGridMimetypes.COLLECTION);

    String rdf = COLLECTION_TEMPLATE_START;
    for (String uri : uris) {
      // Take away the revision, if existing.
      uri = uri.replace(".0", "");
      rdf += "        <ore:aggregates rdf:resource=\"" + uri + "\"/>\n";
    }
    rdf += COLLECTION_TEMPLATE_END;

    DataHandler objectData =
        new DataHandler(new ByteArrayDataSource(rdf.getBytes(), "application/octet-stream"));

    TextGridObject tgObject = new TextGridObject();
    tgObject.setMetadatada(metadata);
    tgObject.setData(objectData.getInputStream());

    // JAXB.marshal(metadata, System.out);
    // System.out.println(IOUtils.readStringFromStream(tgObject.getData()));

    // TODO See <https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients/-/issues/2>.
    if (!this.dryRun) {
      CreateOperation create = IdiomTextGridObject.crudClient.create();
      String tguri = create.setSid(this.sessionID)
          .setProjectId(this.projectID)
          .setObject(tgObject)
          .execute();
      this.textgridURI = tguri;
    } else {
      this.generateFakeTextGridURI();
    }
  }

  // **
  // INTERNAL METHODS
  // **

  private static MetadataContainerType createMetadata(String title, String format)
      throws UnsupportedEncodingException {

    MetadataContainerType result = JAXB.unmarshal(
        new ByteArrayInputStream(METADATA_TEMPLATE.getBytes("UTF-8")), MetadataContainerType.class);

    result.getObject().getGeneric().getProvided().getTitle().set(0, title);
    result.getObject().getGeneric().getProvided().setFormat(format);

    return result;
  }

  /**
  * 
  */
  private void generateFakeTextGridURI() {
    String fullUuid = UUID.randomUUID().toString();
    String uuid = fullUuid.substring(fullUuid.lastIndexOf("-") + 1);
    this.textgridURI = uuid + ".0";
  }

  /**
   * @param theAggregation
   * @return
   */
  private static int getResourceCount(String theAggregation) {

    int result = 0;

    String entries[] = theAggregation.split("<ore:aggregates rdf:resource=");

    if (entries.length > 0) {
      result = entries.length - 1;
    }

    return result;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public String getTextgridURI() {
    return this.textgridURI;
  }

  /**
   * @param textgridURI
   */
  public void setTextgridURI(String textgridURI) {
    this.textgridURI = textgridURI;
  }

  /**
   * @return
   */
  public String getTextgridURL() {
    return this.textgridURL;
  }

  /**
   * @param textgridURL
   */
  public void setTextgridURL(String textgridURL) {
    this.textgridURL = textgridURL;
  }

  /**
   * @return
   */
  public String getSessionID() {
    return this.sessionID;
  }

  /**
   * @param sessionID
   */
  public void setSessionID(String sessionID) {
    this.sessionID = sessionID;
  }

  public String getProjectID() {
    return this.projectID;
  }

  /**
   * @param projectID
   */
  public void setProjectID(String projectID) {
    this.projectID = projectID;
  }

  /**
   * @return
   */
  public String getFormatToFilter() {
    return this.formatToFilter;
  }

  /**
   * @param formatToFilter
   */
  public void setFormatToFilter(String formatToFilter) {
    this.formatToFilter = formatToFilter;
  }

  /**
   * @return
   */
  public String getQuerySize() {
    return this.querySize;
  }

  /**
   * @param querySize
   */
  public void setQuerySize(String querySize) {
    this.querySize = querySize;
  }

  /**
   * @return
   */
  public String getConedaKorMediumMetadataCollectionURI() {
    return this.conedaKorMediumMetadataCollectionURI;
  }

  /**
   * @param conedaKorMediumMetadataCollectionURI
   */
  public void setConedaKorMediumMetadataCollectionURI(
      String conedaKorMediumMetadataCollectionURI) {
    this.conedaKorMediumMetadataCollectionURI = conedaKorMediumMetadataCollectionURI;
  }

  /**
   * @return
   */
  public int getMaxConedaKorCollectionSize() {
    return this.maxConedaKorCollectionSize;
  }

  /**
   * @param maxConedaKorCollectionSize
   */
  public void setMaxConedaKorCollectionSize(int maxConedaKorCollectionSize) {
    this.maxConedaKorCollectionSize = maxConedaKorCollectionSize;
  }

  /**
   * @return
   */
  public boolean isDryRun() {
    return this.dryRun;
  }

  /**
   * @param dryRun
   */
  public void setDryRun(boolean dryRun) {
    this.dryRun = dryRun;
  }

  /**
   * @return
   */
  public String getScope() {
    return this.scope;
  }

  /**
   * @param scope
   */
  public void setScope(String scope) {
    this.scope = scope;
  }

  /**
   * @return
   */
  public String getTextgridCrudEndpoint() {
    return this.textgridCrudEndpoint;
  }

  /**
   * @param textgridCrudEndpoint
   */
  public void setTextgridCrudEndpoint(String textgridCrudEndpoint) {
    this.textgridCrudEndpoint = textgridCrudEndpoint;
  }

  /**
   * @return
   */
  public String getDate() {
    return this.date;
  }

  /**
   * @param date
   */
  public void setDate(String date) {
    this.date = date;
  }

}
