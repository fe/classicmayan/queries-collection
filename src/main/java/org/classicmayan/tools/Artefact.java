package org.classicmayan.tools;

/**
 *
 */
public class Artefact {

  private String textGridUri;
  private String preferredTitle;
  private String idiomNumber;
  private String artefactType;
  private String material;
  private String discoveryPlace;
  private Identifier identifier;
  private Place place;
  private String dedicationDate;
  private String dedicationGregDate;

  /**
   * @param textGridUri
   */
  public Artefact(String textGridUri) {
    this.textGridUri = textGridUri;
    this.setPreferredTitle(TripleStoreQuery.getArtefactNameByTextGridUri(textGridUri));
  }

  /**
   * @return
   */
  public String getDedicationGregDate() {
    return this.dedicationGregDate;
  }

  /**
   * @param dedicationGregDate
   */
  public void setDedicationGregDate(String dedicationGregDate) {
    this.dedicationGregDate = dedicationGregDate;
  }

  /**
   * @return
   */
  public String getDedicationDate() {
    return this.dedicationDate;
  }

  /**
   * @param dedicationDate
   */
  public void setDedicationDate(String dedicationDate) {
    this.dedicationDate = dedicationDate;
  }

  /**
   * @return
   */
  public Place getPlace() {
    return this.place;
  }

  /**
   * @param place
   */
  public void setPlace(Place place) {
    this.place = place;
  }

  /**
   * 
   */
  public Artefact() {

  }

  /**
   * @return
   */
  public Identifier getIdentifier() {
    return this.identifier;
  }

  /**
   * @param identifier
   */
  public void setIdentifier(Identifier identifier) {
    this.identifier = identifier;
  }

  /**
   * @return
   */
  public String getDiscoveryPlace() {
    return this.discoveryPlace;
  }

  /**
   * @param discoveryPlace
   */
  public void setDiscoveryPlace(String discoveryPlace) {
    this.discoveryPlace = discoveryPlace;
  }

  /**
   * @return
   */
  public String getMaterial() {
    return this.material;
  }

  /**
   * @param material
   */
  public void setMaterial(String material) {
    this.material = material;
  }

  /**
   * @return
   */
  public String getArtefactType() {
    return this.artefactType;
  }

  /**
   * @param artefactType
   */
  public void setArtefactType(String artefactType) {
    this.artefactType = artefactType;
  }

  /**
   * @return
   */
  public String getIdiomNumber() {
    return this.idiomNumber;
  }

  /**
   * @param idiomNumber
   */
  public void setIdiomNumber(String idiomNumber) {
    this.idiomNumber = idiomNumber;
  }

  /**
   * @return
   */
  public String getTextGridUri() {
    return this.textGridUri;
  }

  /**
   * @param textGridUri
   */
  public void setTextGridUri(String textGridUri) {
    this.textGridUri = textGridUri;
  }

  /**
   * @return
   */
  public String getPreferredTitle() {
    return this.preferredTitle;
  }

  /**
   * @param preferredTitle
   */
  public void setPreferredTitle(String preferredTitle) {
    this.preferredTitle = preferredTitle;
  }

}
