package org.classicmayan.tools;

/**
 *
 */
public class EpigraphicGroup {

  private Identifier identifier;
  private String groupName;
  private String gndRef;
  private String residenceTitle;

  /**
   * @return
   */
  public Identifier getIdentifier() {
    return this.identifier;
  }

  /**
   * @return
   */
  public String getResidenceTitle() {
    return this.residenceTitle;
  }

  /**
   * @param residenceTitle
   */
  public void setResidenceTitle(String residenceTitle) {
    this.residenceTitle = residenceTitle;
  }

  /**
   * @return
   */
  public String getGndRef() {
    return this.gndRef;
  }

  /**
   * @param gndRef
   */
  public void setGndRef(String gndRef) {
    this.gndRef = gndRef;
  }

  /**
   * @return
   */
  public String getGroupName() {
    return this.groupName;
  }

  /**
   * @param groupName
   */
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  /**
   * @param identifier
   */
  public void setIdentifier(Identifier identifier) {
    this.identifier = identifier;
  }

}
