package org.classicmayan.tools;

/**
 *
 */
public class Activity {

  private String title;
  private Identifier identifier;
  private String placeName;
  private Date datesInDifferentFormats;

  /**
   * @return
   */
  public String getPlaceName() {
    return this.placeName;
  }

  /**
   * @return
   */
  public Date getDatesInDifferentFormats() {
    return this.datesInDifferentFormats;
  }

  /**
   * @param datesInDifferentFormats
   */
  public void setDatesInDifferentFormats(Date datesInDifferentFormats) {
    this.datesInDifferentFormats = datesInDifferentFormats;
  }

  /**
   * @return
   */
  public Identifier getIdentifier() {
    return this.identifier;
  }

  /**
   * @param identifier
   */
  public void setIdentifier(Identifier identifier) {
    this.identifier = identifier;
  }

  /**
   * @return
   */
  public String getTitle() {
    return this.title;
  }

  /**
   * @param title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @param placeName
   */
  public void setPlaceName(String placeName) {
    this.placeName = placeName;
  }

}
