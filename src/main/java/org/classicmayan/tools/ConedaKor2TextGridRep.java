/**
 * This software is copyright (c) 2023 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.ws.rs.NotFoundException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;

/**
 * <p>
 * Import and update of ConedaKOR image metadata to TextGridRep OwnStorage (TG-lab).
 * </p>
 */
public class ConedaKor2TextGridRep {

  // ################################################################################
  // ## Please set config filename in:
  // ## /idiomQueries/src/main/java/org/classicmayan/tools/IdiomConstants.java
  // ################################################################################

  // **
  // FINALS
  // **

  // Name of the root collection in TextGrid to store ConedaKOR image metadata to.
  private static final String MEDIUM_METADATA_COLLECTION_NAME = "ConedaKorMediumMetadata";
  // 500 seems to be maximum for ConedaKOR REST API anyway.
  private static final int OBJECTS_PER_PAGE = 500;
  // Set to ConedaKOR response page to start with searching and importing objects.
  private static final int IMPORT_START_PAGE = 1;

  // **
  // STATICS
  // **

  protected static Logger log = Logger.getLogger(ConedaKor2TextGridRep.class.getName());

  // **
  // CLASS
  // **

  private TGSearchQueries tgsearch;
  private IdiomTextGridObject itgo;

  private ObjectType latestSubcollectionObject;
  private String latestSubcollectionURI;
  private String latestSubcollectionTitle;
  private int latestSubcollectionSize;
  private String titleOfNewSubcollection;
  private int amountOfEntriesLeftInLastCollection;

  private String latestObjectURI;
  private String latestObjectTitle;

  private String timestamp;
  private int maxObjectsPerSubcol;
  private String propertiesFileLocation = IdiomConstants.PROPERTIES_FILE;

  /**
   * @throws IOException
   * @throws CrudClientException
   * @throws ProtocolNotImplementedFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws JSONException
   * @throws InterruptedException
   */
  public static void main(String[] args) throws IOException, CrudClientException, JSONException,
      ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault, ProtocolNotImplementedFault,
      InterruptedException {

    ConedaKor2TextGridRep main = new ConedaKor2TextGridRep();

    /*
     * PREPARATIONS.
     */

    main.preparations();

    /*
     * IMPORT OF ALL CONEDAKOR OBJECTS IN TEXTGRIDREP.
     */

    if (main.itgo.getScope().equals(IdiomConstants.SCOPE_IMPORT_ALL)) {
      main.importAll();
    }

    /*
     * CHECK ALL KOR IDS IN TEXTGRIDLAB AND IMPORT MISSING CONEDAKOR OBJECTS.
     */

    else if (main.itgo.getScope().equals(IdiomConstants.SCOPE_IMPORT_MISSING)) {
      main.importMissing();
    }

    /*
     * UPDATE OF MODIFIED CONEDAKOR OBJECTS EXISTING IN TEXTGRIDREP.
     */

    else if (main.itgo.getScope().equals(IdiomConstants.SCOPE_UPDATE)) {
      main.updateExisting();
    }

    /*
     * IMPORT OF NEW CONEDAKOR OBJECTS IN TEXTGRIDREP.
     */

    else if (main.itgo.getScope().equals(IdiomConstants.SCOPE_IMPORT_NEW)) {
      main.importNew();
    }

    /*
     * UPDATE AND IMPORT NEW CONEDAKOR OBJECTS IN TEXTGRIDREP.
     */

    else if (main.itgo.getScope().equals(IdiomConstants.SCOPE_UPDATE_AND_IMPORT_NEW)) {
      main.updateExisting();
      main.importNew();
    }

    /*
     * ERROR.
     */

    else {
      System.exit(1);
    }
  }

  /**
   * <p>
   * Prepare vars and objects.
   * </p>
   * 
   * @param propertiesFile
   * @throws IOException
   * @throws CrudClientException
   */
  public void preparations() throws IOException, CrudClientException {

    this.tgsearch = new TGSearchQueries(this.propertiesFileLocation);
    this.itgo = new IdiomTextGridObject(this.propertiesFileLocation);
    this.timestamp = this.itgo.getDate();
    this.maxObjectsPerSubcol = this.itgo.getMaxConedaKorCollectionSize();
    // Check for URI, title, and size of latest subcollection in root metadata collection and
    // related objects.
    this.latestSubcollectionObject = this.tgsearch
        .getLatestSubcollectionObject(this.itgo.getConedaKorMediumMetadataCollectionURI());
    this.latestSubcollectionURI =
        this.latestSubcollectionObject.getGeneric().getGenerated().getTextgridUri().getValue();

    // Check for null title (then SID is most likely invalid!)
    ProvidedType p = this.latestSubcollectionObject.getGeneric().getProvided();
    if (p == null) {
      String message =
          "TextGrid metadata can not be fetched! Maybe your configuration is invalid (please check for valid RBAC Session ID in configuration!)";
      throw new IOException(message);
    }
    this.latestSubcollectionTitle = p.getTitle().get(0);
    this.latestSubcollectionSize = this.tgsearch.getChildren(this.latestSubcollectionURI);

    // Check for URI (and title) of latest entry in latest subcollection.
    this.latestObjectURI = this.tgsearch.getLatestObjectURI(this.latestSubcollectionURI);
    this.latestObjectTitle = this.tgsearch.getMetadata(this.latestObjectURI).getGeneric()
        .getProvided().getTitle().get(0);
    // Get timestamp from latest TextGrid subcollection object, if empty in config.
    if (this.timestamp == null || this.timestamp.isEmpty()) {
      this.timestamp = this.tgsearch.getMetadata(this.latestObjectURI).getGeneric()
          .getGenerated().getCreated().toString();
    }

    // New subcollection title and more...
    this.amountOfEntriesLeftInLastCollection =
        this.itgo.getMaxConedaKorCollectionSize() - this.latestSubcollectionSize;
    this.titleOfNewSubcollection =
        String.valueOf(Integer.parseInt(this.latestSubcollectionTitle) + 1);

    // Print configuration :-)
    printConfiguration();
  }

  /**
   * <p>
   * IMPORT ALL image object metadata from ConedaKOR.
   * </p>
   * 
   * <p>
   * <strong>NOTE</strong>! If you set "amount" to a lesser number as there are objects, only this
   * amount of data is imported to TextGrid!
   * </p>
   * 
   * @throws JSONException
   * @throws IOException
   * @throws InterruptedException
   * @throws CrudClientException
   */
  public void importAll()
      throws JSONException, IOException, InterruptedException, CrudClientException {

    // **
    // GATHER ALL OBJECTS.
    // **

    log.info("IMPORT all ConedaKOR image objects in TG-rep");

    // Get list of all objects in ConedaKOR.
    log.info("\tLooking for ALL images");
    log.info("\tConedaKOR URL: " + ConedaKorQueries.KOR_URL);

    // Check amount.
    JSONObject testMediaList = ConedaKorQueries.getAllImageMetadata(10, 1);
    int amount = testMediaList.getInt("total");
    // NOTE Change amount by hand here to import only a few or a few more, but not all, images!
    // amount = 3109;
    int pages = amount / OBJECTS_PER_PAGE;
    // Add last page if existing.
    int rest = amount - pages * OBJECTS_PER_PAGE;
    if (rest > 0) {
      pages++;
    }

    log.info("\tFound " + amount + " entries delivered in " + pages + " pages!");

    // **
    // LOOP AND CREATE NEW TG OBJECTS.
    // **

    List<String> uriList = new ArrayList<String>();
    for (int p = IMPORT_START_PAGE; p <= pages; p++) {

      int objectsToFetch = OBJECTS_PER_PAGE;
      // Check if amount was set by hand...
      if (p == pages) {
        objectsToFetch = rest;
      }
      JSONArray mediaList =
          ConedaKorQueries.getAllImageMetadata(objectsToFetch, p).getJSONArray("records");

      int startKID = mediaList.getJSONObject(0).getInt("id");
      int endKID = mediaList.getJSONObject(mediaList.length() - 1).getInt("id");
      log.info("\tGetting next page " + p + " --> KID:" + startKID + "-KID:" + endKID);

      // Create new TextGrid objects.
      uriList.addAll(createNewTGObject(mediaList, this.propertiesFileLocation));
    }

    log.info("\tURI list (size: " + uriList.size() + "): " + uriList);

    // **
    // CREATE COLLECTIONS.
    // **

    List<String> subcols = createCollections(uriList.subList(0, amount));

    // Create ConedaKOR image metadata root collection.
    this.itgo.createCollection(subcols, MEDIUM_METADATA_COLLECTION_NAME);

    String mainCollectionURI = this.itgo.getTextgridURI();

    log.info("\tCreated root collection " + mainCollectionURI + " with title '"
        + MEDIUM_METADATA_COLLECTION_NAME + "' and " + subcols.size() + " objects");
  }

  /**
   * <p>
   * CHECKS aLL image object metadata from ConedaKOR if already existing in TextGridLab, and ADDS
   * it, if missing.
   * </p>
   * 
   * @throws JSONException
   * @throws IOException
   * @throws InterruptedException
   * @throws CrudClientException
   */
  public void importMissing()
      throws JSONException, IOException, InterruptedException, CrudClientException {

    // **
    // CHECK AND GATHER MISSING OBJECTS.
    // **

    log.info("CHECK all ConedaKOR image objects in TG-rep");

    // Get list of all objects in ConedaKOR.
    log.info("\tLooking for ALL images");
    log.info("\tConedaKOR URL: " + ConedaKorQueries.KOR_URL);

    // Check amount.
    JSONObject testMediaList = ConedaKorQueries.getAllImageMetadata(10, 1);
    int amount = testMediaList.getInt("total");
    int pages = amount / OBJECTS_PER_PAGE;
    // Add last page if existing.
    int rest = amount - pages * OBJECTS_PER_PAGE;
    if (rest > 0) {
      pages++;
    }

    log.info("\tFound " + amount + " entries delivered in " + pages + " pages!");

    List<String> missingKorIDs = new ArrayList<String>();
    JSONArray missingJSONs = new JSONArray();
    // Loop!
    for (int p = IMPORT_START_PAGE; p <= pages; p++) {

      int objectsToFetch = OBJECTS_PER_PAGE;
      // Check if amount was set by hand...
      if (p == pages) {
        objectsToFetch = rest;
      }
      JSONArray mediaList =
          ConedaKorQueries.getAllImageMetadata(objectsToFetch, p).getJSONArray("records");
      int startKID = mediaList.getJSONObject(0).getInt("id");
      int endKID = mediaList.getJSONObject(mediaList.length() - 1).getInt("id");
      log.info("\tGetting next page " + p + " --> KID:" + startKID + "-KID:" + endKID);

      // Check TextGrid objects.
      missingKorIDs.addAll(checkForTextGridExistance(mediaList, this.tgsearch));

      // Loop all objects, look for missing KOR IDs, add JSON of missing objects to missing JSON
      // array.
      for (int o = 0; o < mediaList.length(); o++) {
        String kid = String.valueOf(mediaList.getJSONObject(o).getInt("id"));
        if (missingKorIDs.contains(kid)) {
          missingJSONs.put(mediaList.getJSONObject(o));
        }
      }
    }

    // Sort list alphabetically.
    Collections.sort(missingKorIDs);

    log.info(
        "\t" + missingKorIDs.size() + " KOR IDs are missing in TextGrid  (" + missingKorIDs.get(0)
            + "-" + missingKorIDs.get(missingKorIDs.size() - 1) + "): " + missingKorIDs);
    log.info("\tMissing JSON array size: " + missingJSONs.length());

    // **
    // IMPORT MISSING OBJECTS IN TG.
    // **

    List<String> uriList = createNewTGObject(missingJSONs, this.propertiesFileLocation);

    // Update existing subcollections and root image metadata collection.
    updateExistingCollections(uriList);
  }

  /**
   * <p>
   * Get MODIFIED image metadata from ConedaKOR and update existing objects in TextGrid Repository.
   * </p>
   * 
   * <p>
   * <strong>NOTE</strong>! If you want to update more objects (500 max. are taken from ConedaKOR),
   * please just run this again and re-set the timestamp (tg.date in config file)!
   * </p>
   * 
   * @return
   * @throws JSONException
   * @throws IOException
   * @throws CrudClientException
   */
  public String updateExisting() throws JSONException, IOException, CrudClientException {

    String result = this.itgo.getProjectID() + "#updateExisting"
        + (this.itgo.isDryRun() ? " [DRY_RUN]" : "") + " --> ";

    // **
    // GATHER OBJECTS TO UPDATE.
    // **

    log.info("UPDATE up to " + OBJECTS_PER_PAGE + " modified objects in TG-rep");

    // Get list of existing and updated image metadata from ConedaKOR.
    log.info("\tLooking for images created before " + this.timestamp + " and updated after "
        + this.timestamp);
    log.info("\tConedaKOR URL: " + ConedaKorQueries.KOR_URL);

    JSONArray updatedMediaList = ConedaKorQueries
        .getImageMetadataUpdates(this.timestamp, OBJECTS_PER_PAGE).getJSONArray("records");

    // Test only the first... used for productive testing only... :-D
    //
    // JSONArray urg = new JSONArray();
    // urg.put(updatedMediaList.get(0));
    // updatedMediaList = urg;

    String message = "Found " + updatedMediaList.length() + " modified images.";

    log.info("\t" + message);

    if (updatedMediaList.isEmpty()) {
      return result + "Nothing to update.";
    }

    // Get URI mapping ConedaKOR ID <--> TextGrid URI.
    log.info("\tCreating URI mapping");

    Map<String, String> uriMapping = getMapOfModifiedMetadata(this.tgsearch, updatedMediaList);

    log.info("\tURI mapping created: " + updatedMediaList);

    // **
    // UPDATE TG OBJECTS.
    // **

    updateModifiedMetadata(updatedMediaList, uriMapping, this.propertiesFileLocation);

    if (uriMapping.isEmpty()) {
      result += "??UNEXPECTED ERROR?? Please consult your system administrator!";
    } else {
      result += message + " Objects updated: " + uriMapping.entrySet().toString();
    }

    return result;
  }

  /**
   * <p>
   * Get NEW image metadata from ConedaKOR and create new objects in TextGrid Repository project:
   * Idiom, collection: ConedaKorMediumMetadata).
   * </p>
   * 
   * @return
   * @throws JSONException
   * @throws IOException
   * @throws CrudClientException
   */
  public String importNew() throws JSONException, IOException, CrudClientException {

    String result = this.itgo.getProjectID() + "#importNew"
        + (this.itgo.isDryRun() ? " [DRY_RUN]" : "") + " --> ";

    // **
    // CHECK AND GATHER NEW OBJECTS.
    // **

    log.info("CREATE up to " + OBJECTS_PER_PAGE + " new ConedaKOR objects in TG-rep");

    // Get list of new images from ConedaKOR.
    log.info("\tLooking for new image metadata in ConedaKOR since " + this.timestamp);

    JSONArray newMediaList = ConedaKorQueries
        .getLatestImageMetadataCreations(this.timestamp, OBJECTS_PER_PAGE).getJSONArray("records");

    String message = "Found " + newMediaList.length() + " new images.";

    log.info("\t" + message);

    if (newMediaList.isEmpty()) {
      return result + "Nothing new to import.";
    }

    // **
    // CREATE NEW TG OBJECTS.
    // **

    List<String> uriList = createNewTGObject(newMediaList, this.propertiesFileLocation);

    // Update existing subcollections and root image metadata collection.
    updateExistingCollections(uriList);

    if (uriList.isEmpty()) {
      result += "??UNEXPECTED ERROR?? Please consult your system administrator!";
    } else {
      result += message + " Objects created: " + uriList.toString();
    }

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param idiomTextGridObject
   * @param tgSearchObject
   * @throws IOException
   */
  private String printConfiguration() throws IOException {

    StringWriter result = new StringWriter();

    result.append("\n");
    result.append("ConedaKOR --> TextGridRep configuration: " + this.propertiesFileLocation + "\n");
    result.append("\tSCOPE: " + this.itgo.getScope() + "\n");
    result.append("\tDRY_RUN: " + this.itgo.isDryRun() + "\n");
    result.append("\tTextGrid CRUD endpoint:   <" + this.itgo.getTextgridCrudEndpoint() + ">\n");
    result
        .append(
            "\tTextGrid SEARCH endpoint: <" + this.tgsearch.getTextgridSearchEndpoint() + ">\n");
    result.append("\tTextGrid project ID:      " + this.itgo.getProjectID() + "\n");
    result.append(
        "\tConedaKOR collection URI: " + this.itgo.getConedaKorMediumMetadataCollectionURI()
            + "\n");
    result.append("\tLast migration timestamp: " + this.timestamp + "\n");
    result
        .append("\tConedaKOR max col size:   " + this.itgo.getMaxConedaKorCollectionSize() + "\n");
    result.append("\n");
    result.append("Get needed information from TG-rep\n");
    result.append("\tLatest subcollection URI: " + this.latestSubcollectionURI + "\n");
    result.append("\tLatest subcollection title: " + this.latestSubcollectionTitle + "\n");
    result.append("\tLatest subcollection size: " + this.latestSubcollectionSize + "\n");
    result.append("\n");
    result.append("\tLatest object URI: " + this.latestObjectURI + "\n");
    result.append("\tLatest object title: " + this.latestObjectTitle + "\n");
    result.append("\n");
    result.append("\tSpace in latest subcol: " + this.amountOfEntriesLeftInLastCollection + "\n");
    result.append("\tNext subcol title: " + this.titleOfNewSubcollection);

    String s = result.toString();
    result.close();

    log.info(s);

    return s;
  }

  /**
   * <p>
   * Get TextGrid URIs of modified image metadata from TG-search <notes> query with korID.
   * </p>
   * 
   * @param tgSearchObject
   * @param modifiedMetadata
   * @return
   */
  private static Map<String, String> getMapOfModifiedMetadata(TGSearchQueries tgSearchObject,
      JSONArray modifiedMetadata) {

    Map<String, String> result = new HashMap<String, String>();

    // Check mapping first!
    List<String> errorList = new ArrayList<String>();
    for (int i = 0; i < modifiedMetadata.length(); i++) {
      JSONObject jsonObjectMetadata = modifiedMetadata.getJSONObject(i);
      String korID = String.valueOf(jsonObjectMetadata.getInt("id"));

      try {
        String textgridURI = tgSearchObject.getTextGridURIfromKorID(korID);

        log.info("\t[" + (i + 1) + "] " + korID + " --> " + textgridURI);

        result.put(korID, textgridURI);

      } catch (NotFoundException e) {
        errorList.add(korID);
        log.severe("!ERROR! " + e.getMessage()
            + " Please check <notes> and <title> tag for KOR ID [" + korID + "]");
      }
    }

    if (!errorList.isEmpty()) {
      String message = "--->  !ERROR! Please check KOR ID" + (errorList.size() != 1 ? "s" : "")
          + ": " + errorList.toString() + "  <---";
      log.severe(message);
    }

    return result;
  }

  /**
   * <p>
   * Update changed ConedaKOR image metadata objects to TextGrid.
   * </p>
   * 
   * @param modifiedMetadata
   * @param uriMapping
   * @param propertiesFile
   * @throws IOException
   * @throws CrudClientException
   */
  private static void updateModifiedMetadata(JSONArray modifiedMetadata,
      Map<String, String> uriMapping, String propertiesFile)
      throws IOException, CrudClientException {

    log.info("\tUpdating TextGrid objects");

    for (int i = 0; i < modifiedMetadata.length(); i++) {
      JSONObject jsonObjectMetadata = modifiedMetadata.getJSONObject(i);

      String korID = String.valueOf(jsonObjectMetadata.getInt("id"));

      // Create IDIOM TextGrid object and SET existing TextGrid URI!
      String uri = uriMapping.get(korID);
      if (uri != null && !uri.isEmpty()) {
        IdiomTextGridObject transfer = new IdiomTextGridObject(propertiesFile);
        transfer.setTextgridURI(uri);

        // Update every single metadata object.
        transfer.updateConedaKorMediumToTextGrid(jsonObjectMetadata);
      } else {
        log.info("\t[" + (i + 1) + "] " + korID + " --> skipping [NO URI]");
      }
    }
  }

  /**
   * <p>
   * Create new TextGrid objects.
   * </p>
   * 
   * @param newMetadata
   * @param propertiesFile
   * @return
   * @throws IOException
   * @throws CrudClientException
   */
  private static List<String> createNewTGObject(JSONArray newMetadata, String propertiesFile)
      throws IOException, CrudClientException {

    log.info("Creating " + newMetadata.length() + " new TextGrid objects");

    List<String> result = new ArrayList<String>();

    for (int i = 0; i < newMetadata.length(); i++) {

      JSONObject jsonObjectMetadata = newMetadata.getJSONObject(i);

      String korID = String.valueOf(jsonObjectMetadata.getInt("id"));

      log.fine("\t[" + (i + 1) + "] " + korID + " --> ");

      // Create IDIOM TextGrid object.
      IdiomTextGridObject transfer = new IdiomTextGridObject(propertiesFile);

      // Transfer every single metadata object to TextGrid.
      transfer.transferConedaKorMediumToTextGrid(jsonObjectMetadata);

      // Add TextGrid URI to URI list.
      result.add(transfer.getTextgridURI());
    }

    log.info("\tList of " + result.size() + " new TextGrid URIs: " + result);

    return result;
  }

  /**
   * <p>
   * Check for existence of the ConedaKOR objects in TextGridLab.
   * </p>
   * 
   * @param newMetadata
   * @throws IOException
   * @throws CrudClientException
   */
  private static List<String> checkForTextGridExistance(JSONArray newMetadata,
      TGSearchQueries tgsearchClient) {

    List<String> result = new ArrayList<String>();

    for (int i = 0; i < newMetadata.length(); i++) {
      JSONObject jsonObjectMetadata = newMetadata.getJSONObject(i);
      String korID = String.valueOf(jsonObjectMetadata.getInt("id"));

      // Check for KOR ID in TextGrid titles.
      try {
        String textgridURI = tgsearchClient.getTextGridURIfromKorID(korID);
        log.info("\t[" + (i + 1) + "] " + korID + " --> " + textgridURI);
      } catch (NotFoundException e) {
        result.add(korID);
        log.severe("[ERROR] " + e.getMessage());
      }
    }

    return result;
  }

  /**
   * <p>
   * Create a new collection with resources from the given URI list and title.
   * </p>
   * 
   * @param uriList
   * @param pages
   * @throws CrudClientException
   * @throws IOException
   */
  private List<String> createCollections(List<String> uriList)
      throws IOException, CrudClientException {

    List<String> result = new ArrayList<String>();

    int amount = uriList.size();

    // Create collections.
    int collections = amount / this.maxObjectsPerSubcol;
    // Add last page if existing.
    int rest = amount - collections * this.maxObjectsPerSubcol;
    if (rest > 0) {
      collections++;
    }

    log.info("Creating " + collections + " Collections for " + amount + " objects");

    // Loop!
    for (int i = 1; i <= collections; i++) {
      int start = (i - 1) * this.maxObjectsPerSubcol;
      int end = i * this.maxObjectsPerSubcol;
      if (end > amount) {
        end = amount;
      }
      String name = String.valueOf(i);
      List<String> sublist = uriList.subList(start, end);

      this.itgo.createCollection(sublist, String.valueOf(i));
      String collectionURI = this.itgo.getTextgridURI();
      result.add(collectionURI);

      log.info("\tCreated new collection " + collectionURI + " with title '" + name
          + "' and " + sublist.size() + " objects with index " + start + "-" + (end - 1));
    }

    return result;
  }

  /**
   * @param uriList
   * @throws CrudClientException
   * @throws IOException
   */
  private void updateExistingCollections(List<String> uriList)
      throws CrudClientException, IOException {

    log.info("UPDATE subcollections in TG-rep");

    // Less or equal new amount of objects than space is left in latest subcollection!
    if (uriList.size() <= this.amountOfEntriesLeftInLastCollection) {

      log.info("\t" + uriList.size() + " new objects fit in latest collection with space for "
          + this.amountOfEntriesLeftInLastCollection + " objects");

      // Add to latest subcollection only.
      this.itgo.addUriListToAggregation(uriList, this.latestSubcollectionURI);
    }

    // More new objects than space is left in latest subcollection!
    else {

      log.info("\t" + uriList.size() + " new objects doesn't fit in last collection with space for "
          + this.amountOfEntriesLeftInLastCollection + " objects)");

      // Add to latest subcollection only, if space left.
      int addToNewListPastIndex = 0;
      if (this.latestSubcollectionSize < this.maxObjectsPerSubcol) {

        log.info(
            "\t" + uriList.size() + " new objects doesn't fit in last collection with space for "
                + this.amountOfEntriesLeftInLastCollection + " objects)");

        List<String> latestSubcollectionList =
            uriList.subList(0, this.amountOfEntriesLeftInLastCollection);
        this.itgo.addUriListToAggregation(latestSubcollectionList, this.latestSubcollectionURI);
        addToNewListPastIndex = this.amountOfEntriesLeftInLastCollection;
      }
      // Create new and add to new subcollection.
      List<String> newSubcollectionList = uriList.subList(addToNewListPastIndex, uriList.size());
      this.itgo.createCollection(newSubcollectionList, this.titleOfNewSubcollection);

      // Add new subcollection to root collection.
      List<String> newSubcollection = new ArrayList<String>();
      newSubcollection.add(this.itgo.getTextgridURI());
      this.itgo.addUriListToAggregation(newSubcollection,
          this.itgo.getConedaKorMediumMetadataCollectionURI());
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return
   */
  public String getPropertiesFileLocation() {
    return this.propertiesFileLocation;
  }

  /**
   * @param propertiesFileLocation
   */
  public void setPropertiesFileLocation(String propertiesFileLocation) {
    this.propertiesFileLocation = propertiesFileLocation;
  }

}
