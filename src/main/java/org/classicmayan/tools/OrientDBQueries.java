/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 *
 */
public class OrientDBQueries {

  private OrientDB orientDB = new OrientDB("remote:classicmayan.org", null);
  private ODatabaseDocument db;
  private String orientDBName = "classicmayan";
  private String orientDBUser = "functionUser";
  private String orientDBAccess;

  /**
   * 
   */
  public OrientDBQueries() {

  }

  /**
   * @param json
   */
  public void createNewDocument(String json) {

    // Open the database with name classicmayan as user functionUser with password read from config
    // file.
    this.db = this.orientDB.open(this.orientDBName, this.orientDBUser, this.getOrientDBAccess());

    // Create a new document with class "LinguisticAnnotation".
    // If the given class not exists, the class will be created automatically.
    ODocument newDocument = new ODocument("LinguisticAnnotationBLA");
    // Set some properties.
    // TODO Remove hard coded variant!
    newDocument.field("type", "testitest");
    newDocument.field("culture", "hatschi");
    newDocument.field("king", "pakal");
    newDocument.field("textgridURI", "textgrid:666jka613");
    newDocument.setProperty("rid", "456778900");

    // Save the new document into the database. By this step the document will get a valid ID.
    newDocument.save();
    System.out.println(newDocument.getIdentity());
  }

  /**
   * @param orientDBIdentifier
   * @return
   */
  public ODocument readDocument(String orientDBIdentifier) {

    // Open the database with name classicmayan as user functionUser with password read from config
    // file.
    this.db = this.orientDB.open(this.orientDBName, this.orientDBUser, this.getOrientDBAccess());
    ORID theRid = new ORecordId(orientDBIdentifier);
    ODocument doc = this.db.load(theRid);

    return doc;
  }

  /**
   * @param orientDBIdentifier
   */
  public void updateDocument(String orientDBIdentifier) {

    // Open the database with name classicmayan as user functionUser with password read from config
    // file.
    this.db = this.orientDB.open(this.orientDBName, this.orientDBUser, this.getOrientDBAccess());
    ODocument doc = readDocument(orientDBIdentifier);
    doc.field("culture", "atztec");
    doc.save();
  }

  /**
   * @return
   */
  public String getOrientDBAccess() {
    return this.orientDBAccess;
  }

  /**
   * @param orientDBaccess
   */
  public void setOrientDBAccess(String orientDBAccess) {
    this.orientDBAccess = orientDBAccess;
  }

}
