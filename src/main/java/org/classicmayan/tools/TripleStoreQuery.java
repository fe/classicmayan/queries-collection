/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;

/**
 * <p>
 * Class for collecting queries for the SPARQL endpoint.
 * </p>
 */
public class TripleStoreQuery {

  private static final String vocabularyURL =
      "https://www.classicmayan.org/trip/vocabularies/query";
  private static final String queryURL = "https://www.classicmayan.org/trip/metadata/query";

  public static List<String> getArtefactsWithLinksToKor() {
    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?textgridURI WHERE { GRAPH ?artefactURI {\n" +
        " ?textgridURI rdf:type idiom:Artefact.\n" +
        " ?textgridURI idiom:shows ?kordMediaIDs" +
        "  }\n" +
        "}";

    return QueryExecutionList(query, "textgridURI");
  }

  /**
   * @param textgridURI
   * @return
   */
  public static List<String> getLinkedMediaInKorFromArtefact(String textgridURI) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?textgridURI ?korMediaIDs WHERE { GRAPH <" + textgridURI + "> {\n" +
        " ?textgridURI rdf:type idiom:Artefact.\n" +
        " ?textgridURI idiom:shows ?korMediaIDs" +
        "  }\n" +
        "}";

    return QueryExecutionList(query, "korMediaIDs");
  }

  /**
   * @param finalPlaceName
   * @return
   */
  public static List<String> getAllDiscoveriesLinkedToPlace(String finalPlaceName) {
    Map<String, Integer> places = new HashMap<String, Integer>();

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?textgridURI ?placeRef WHERE { GRAPH ?discovery {\n" +
        " ?textgridURI rdf:type idiom:Discovery.\n" +
        " ?textgridURI crm:P7_took_place_at ?placeRef." +
        "  }\n" +
        "}";

    List<String> allDiscoveries = QueryExecutionList(query, "placeRef");
    List<String> discoveriesLinkedToPlace = new ArrayList<String>();
    for (String discovery : allDiscoveries) {
      String placeName =
          getPlaceRescursive(discovery.replace("http://textgridrep.de/", "textgrid:"));

      if (places.get(placeName) == null) {
        places.merge(placeName, 1, Integer::sum);
      } else {
        places.merge(placeName, 1, Integer::sum);
      }
    }

    return discoveriesLinkedToPlace;
  }

  /**
   * @param textgridURI
   * @return
   */
  public static String getPlaceRescursive(String textgridURI) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?tgURI ?nextPlace ?placeName WHERE { GRAPH <" + textgridURI + "> {\n"
        + "    ?tgURI rdf:type crm:E53_Place.\n"
        + "  	?tgURI crm:P87_is_identified_by ?placeNameRef.\n"
        + "    OPTIONAL{?tgURI crm:P89_falls_within ?nextPlace.}\n"
        + "    BIND(IRI(CONCAT(' + \"<\" + ',?placeNameRef,' + \">\" + ')) AS ?placeNameURL)    \n"
        + "    GRAPH ?placeNameURL {\n"
        + "		  ?placeNameRef idiom:placeNameType \"preferred\"."
        + "       ?placeNameRef idiom:placeName ?placeName.  \n"
        + "      }\n"
        + "    }  \n"
        + "  }";

    try {
      String placeName = QueryExecution(query, "?placeName");
      try {
        return getPlaceRescursive(
            QueryExecution(query, "?nextPlace").replace("http://textgridrep.de/", "textgrid:"));
      } catch (NullPointerException nullPontrer) {

        return placeName;
      }
    } catch (NoSuchElementException e) {
      // TODO HANDLE EXCEPTION!
      e.printStackTrace();
    }

    // ÄHM? "DÖPP"? REALLY? :-D
    return "DÖPP";
  }

  /**
   * @return List containing all textgrid URIs in the SPARQL endpoint.
   */
  public static List<String> getArtefactList() {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?textgridURI WHERE { GRAPH ?artefactURI {\n" +
        " ?textgridURI rdf:type idiom:Artefact.\n" +
        "  }\n" +
        "}";

    return QueryExecutionList(query, "textgridURI");
  }

  /**
   * @param textgridURI
   * @return
   */
  public static List<String> getArtefactImages(String textgridURI) {

    String query = IdiomConstants.SPARQL_PREFIXES + "SELECT *  WHERE { GRAPH <" + textgridURI
        + "> {?artefact idiom:shows ?images}}";

    return QueryExecutionList(query, "images");
  }

  /**
   * @param textgridURI Unique identifier in the SPARQL endpoint for a specific sign in the sign
   *        catalogue.
   * @return The sign number of a sign number related to the given textgridURI.
   */
  public static String getSignNumberForUri(String textgridURI) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?signURI ?signNumber WHERE { GRAPH <" + textgridURI + "> {\n" +
        " 	?textgridURI rdf:type idiomcat:Sign.\n" +
        "    ?textgridURI idiomcat:signNumber ?signNumber.\n" +
        "  }\n" +
        "}";

    return QueryExecution(query, "signNumber");
  }

  /**
   * <p>
   * Finds the textgridURI of a given graphNumber.
   * </p>
   * 
   * @param graphNumber The graphNumber of a graph in the sign catalogue
   * @return the textgridURI
   */
  public static String getTextGridUriForGraphNumber(String graphNumber) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?tgUri\n" +
        "  WHERE { GRAPH ?tgUri {\n" +
        "    ?tgObjects rdf:type idiomcat:Graph.\n" +
        "    ?tgObjects idiomcat:graphNumber \"" + graphNumber + "\";\n" +
        "  }\n" +
        "}";

    return QueryExecution(query, "tgUri");
  }

  /**
   * @param graphUri
   * @return
   */
  public static String getIsGraphOfSign(String graphUri) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT * WHERE { GRAPH <" + graphUri + "> {\n" +
        "	?graph idiomcat:isGraphOf ?signUri.\n" +
        "  }\n" +
        "}";

    return QueryExecution(query, "signUri").replace("http://textgridrep.de/", "textgrid:");
  }

  /**
   * @param textgridUri
   * @return
   */
  private static String isGraphOrSign(String textgridUri) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT * WHERE { GRAPH <" + textgridUri + "> {\n" +
        "	?graphOrSign rdf:type ?type.\n" +
        "   }\n" +
        "  FILTER (regex (str(?type), \"Sign\") || regex (str(?type), \"Graph\")  )\n" +
        "}";

    return QueryExecution(query, "type").replace("http://idiom-projekt.de/catalogue/", "");
  }

  /**
   * @param textgridURI
   * @return A key value pair containing the translitrationValue and the related level.
   */
  @SuppressWarnings("rawtypes")
  public static List<Pair> getLogographicReadingTransliterationValueWithLevel(
      final String textgridURI) {

    String uri = textgridURI;

    if (isGraphOrSign(uri).equals("Graph")) {
      uri = getIsGraphOfSign(uri);
    }

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?signURI ?transliterationValue ?level WHERE { GRAPH <" + uri + "> {\n" +
        "    ?textgridURI " + IdiomConstants.SIGN_LOGOGRAPHIC_READING_REF
        + " ?linguisticAnalysisRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?linguisticAnalysisRef,' + \">\" + ')) AS ?linguisticAnalysisURI)\n"
        +
        "    GRAPH ?linguisticAnalysisURI {\n" +
        "      ?linguisticAnalysisRef idiomcat:transliterationValue ?transliterationValue.\n" +
        "      ?linguisticAnalysisRef "
        + IdiomConstants.SIGN_CONFIDENCE_LEVEL_LOGOGRAPHIC_READING + " ?clRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?clRef,' + \">\" + ')) AS ?clURI)\n" +
        "      GRAPH ?clURI {\n" +
        "        ?clRef idiomcat:resultsInLevel ?level.\n" +
        "      }\n" +
        "    }    \n" +
        "  }\n" +
        "}";

    return QueryExecutionForKeyValuePair(query, "transliterationValue", "level");
  }

  /**
   * @param textgridURI
   * @return A key value pair containing the translitrationValue and the related level.
   */
  @SuppressWarnings("rawtypes")
  public static List<Pair> getLogographicMeaningTransliterationValueWithLevel(
      final String textgridURI) {

    String uri = textgridURI;

    if (isGraphOrSign(uri).equals("Graph")) {
      uri = getIsGraphOfSign(uri);
    }
    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?signURI ?transliterationValue ?level WHERE { GRAPH <" + uri + "> {\n" +
        "    ?textgridURI " + IdiomConstants.SIGN_LOGOGRAPHIC_MEANING_REF
        + " ?linguisticAnalysisRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?linguisticAnalysisRef,' + \">\" + ')) AS ?linguisticAnalysisURI)\n"
        +
        "    GRAPH ?linguisticAnalysisURI {\n" +
        "      ?linguisticAnalysisRef idiomcat:transliterationValue ?transliterationValue.\n" +
        "      ?linguisticAnalysisRef "
        + IdiomConstants.SIGN_CONFIDENCE_LEVEL_LOGOGRAPHIC_MEANING + " ?clRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?clRef,' + \">\" + ')) AS ?clURI)\n" +
        "      GRAPH ?clURI {\n" +
        "        ?clRef idiomcat:resultsInLevel ?level.\n" +
        "      }\n" +
        "    }    \n" +
        "  }\n" +
        "}";

    return QueryExecutionForKeyValuePair(query, "transliterationValue", "level");
  }

  /**
   * @param textgridURI
   * @return A key value pair containing the translitrationValue and the related level.
   */
  @SuppressWarnings("rawtypes")
  public static List<Pair> getSyllabicReadingTransliterationValueWithLevel(
      final String textgridURI) {

    String uri = textgridURI;

    if (isGraphOrSign(uri).equals("Graph")) {
      uri = getIsGraphOfSign(uri);
    }

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?signURI ?transliterationValue ?level WHERE { GRAPH <" + uri + "> {\n" +
        "    ?textgridURI " + IdiomConstants.SIGN_SYLLABIC_READING_REF
        + " ?linguisticAnalysisRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?linguisticAnalysisRef,' + \">\" + ')) AS ?linguisticAnalysisURI)\n"
        +
        "    GRAPH ?linguisticAnalysisURI {\n" +
        "      ?linguisticAnalysisRef idiomcat:transliterationValue ?transliterationValue.\n" +
        "      ?linguisticAnalysisRef " + IdiomConstants.SIGN_CONFIDENCE_LEVEL_SYLLABIC_READING
        + " ?clRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?clRef,' + \">\" + ')) AS ?clURI)\n" +
        "      GRAPH ?clURI {\n" +
        "        ?clRef idiomcat:resultsInLevel ?level.\n" +
        "      }\n" +
        "    }    \n" +
        "  }\n" +
        "}";

    return QueryExecutionForKeyValuePair(query, "transliterationValue", "level");
  }

  /**
   * @param textgridURI
   * @return A key value pair containing the translitrationValue and the related level.
   */
  @SuppressWarnings("rawtypes")
  public static List<Pair> getDiacriticFunctionTransliterationValueWithLevel(
      final String textgridURI) {

    String uri = textgridURI;

    if (isGraphOrSign(uri).equals("Graph")) {
      uri = getIsGraphOfSign(uri);
    }

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?signURI ?transliterationValue ?level WHERE { GRAPH <" + uri + "> {\n" +
        "    ?textgridURI " + IdiomConstants.SIGN_DIACRITIC_FUNCTION_REF
        + " ?linguisticAnalysisRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?linguisticAnalysisRef,' + \">\" + ')) AS ?linguisticAnalysisURI)\n"
        +
        "    GRAPH ?linguisticAnalysisURI {\n" +
        "      ?linguisticAnalysisRef idiomcat:transliterationValue ?transliterationValue.\n" +
        "      ?linguisticAnalysisRef "
        + IdiomConstants.SIGN_CONFIDENCE_LEVEL_DIACRITIC_FUNCTION + " ?clRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?clRef,' + \">\" + ')) AS ?clURI)\n" +
        "      GRAPH ?clURI {\n" +
        "        ?clRef idiomcat:resultsInLevel ?level.\n" +
        "      }\n" +
        "    }    \n" +
        "  }\n" +
        "}";

    return QueryExecutionForKeyValuePair(query, "transliterationValue", "level");
  }

  /**
   * @param textgridURI
   * @return A key value pair containing the translitrationValue and the related level.
   */
  @SuppressWarnings("rawtypes")
  public static List<Pair> getNumericFunctionTransliterationValueWithLevel(
      final String textgridURI) {

    String uri = textgridURI;

    if (isGraphOrSign(uri).equals("Graph")) {
      uri = getIsGraphOfSign(uri);
    }

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?signURI ?transliterationValue ?level WHERE { GRAPH <" + uri + "> {\n" +
        "    ?textgridURI " + IdiomConstants.SIGN_NUMERIC_FUNCTION_REF
        + " ?linguisticAnalysisRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?linguisticAnalysisRef,' + \">\" + ')) AS ?linguisticAnalysisURI)\n"
        +
        "    GRAPH ?linguisticAnalysisURI {\n" +
        "      ?linguisticAnalysisRef idiomcat:transliterationValue ?transliterationValue.\n" +
        "      ?linguisticAnalysisRef " + IdiomConstants.SIGN_CONFIDENCE_LEVEL_NUMERIC_FUNCTION
        + " ?clRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?clRef,' + \">\" + ')) AS ?clURI)\n" +
        "      GRAPH ?clURI {\n" +
        "        ?clRef idiomcat:resultsInLevel ?level.\n" +
        "      }\n" +
        "    }    \n" +
        "  }\n" +
        "}";

    return QueryExecutionForKeyValuePair(query, "transliterationValue", "level");
  }

  /**
   * @param textgridUri
   * @return
   */
  public static String getArtefactNameByTextGridUri(final String textgridUri) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT ?artefactTitle WHERE { GRAPH <" + textgridUri + "> {\n" +
        "    ?textgridURI idiom:preferredTitle ?artefactTitle.\n" +
        "  }\n" +
        "} ";

    return QueryExecution(query, "artefactTitle");
  }

  /**
   * <P>
   * Generic function for adding results of a SPARQL query to a list.
   * </p>
   * 
   * @param query
   * @param fieldToGetFromEndpoint
   * @return string list containing textgridURIs
   */
  private static List<String> QueryExecutionList(String query, String fieldToGetFromEndpoint) {

    List<String> resultList = new ArrayList<String>();
    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      RDFNode artefactURI = processQuery.get(fieldToGetFromEndpoint);
      resultList.add(artefactURI.toString());
    }

    return resultList;
  }

  /**
   * @param query
   * @param fieldToGetFromEndpoint
   * @return
   */
  private static String QueryExecution(String query, String fieldToGetFromEndpoint) {

    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();
    QuerySolution processQuery = results.nextSolution();

    return processQuery.get(fieldToGetFromEndpoint).toString();
  }

  /**
   * @param query
   * @param keyFieldToGetFromEndpoint
   * @param valueFieldToGetFromEndpoint
   * @return
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  private static List<Pair> QueryExecutionForKeyValuePair(String query,
      String keyFieldToGetFromEndpoint, String valueFieldToGetFromEndpoint) {

    List<Pair> resultList = new ArrayList<Pair>();

    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      resultList.add(new ImmutablePair(
          processQuery.get(keyFieldToGetFromEndpoint).toString(),
          processQuery.get(valueFieldToGetFromEndpoint).toString()));
    }

    return resultList;
  }

  /**
   * @param keyword
   * @return
   */
  public static List<Artefact> searchArtefactByName(String keyword) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT \n" +
        "          (group_concat( distinct ?tgObject;separator=\"; \") as ?tgObjects)\n" +
        "          (group_concat( distinct ?prefTitle;separator=\"; \") as ?prefTitles)\n" +
        "          (group_concat( distinct ?addIdentifierName;separator=\"; \") as ?addIdentifierNames)\n"
        +
        "          (group_concat( distinct ?addIdentifierType;separator=\"; \") as ?addIdentifierTypes)\n"
        +
        "          (group_concat( distinct ?idiomNumber;separator=\"; \") as ?idiomNumbers)\n" +
        "            WHERE { GRAPH ?textgridURI {\n" +
        "               ?tgObject rdf:type idiom:Artefact .\n" +
        "               ?tgObject idiom:preferredTitle ?prefTitle.\n" +
        "               FILTER (regex (str(?prefTitle), \"" + keyword + "\", \"i\")).\n" +
        "               OPTIONAL { ?tgObject idiom:identifier ?idiomNumber . }\n" +
        "               OPTIONAL {\n" +
        "                ?addIdentifier idiom:noteId ?addIdentifierName .\n" +
        "                ?addIdentifier idiom:identifierType ?addIdentifierType .\n" +
        "               }\n" +
        "            }\n" +
        "    } group by ?tgObject";

    List<Artefact> resultList = new ArrayList<Artefact>();
    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {

      // QuerySolution processQuery = results.nextSolution();
      /*
       * RDFNode artefactURI = processQuery.get("tgObjects"); RDFNode prefTitle =
       * processQuery.get("prefTitles"); RDFNode addIdentifierName =
       * processQuery.get("addIdentifierNames"); RDFNode addIdentifierType =
       * processQuery.get("addIdentifierTypes"); RDFNode idiomNumber =
       * processQuery.get("idiomNumbers");
       */
      // QuerySolution processQuery = results.nextSolution();
      /*
       * RDFNode artefactURI = processQuery.get("tgObjects"); /*resultList.add(new Artefact(
       * processQuery.get("prefTitles").toString(), processQuery.get("tgObjects").toString(),
       * //processQuery.get("addIdentifierNames").toString(),
       * //processQuery.get("addIdentifierTypes").toString(),
       * processQuery.get("idiomNumbers").toString() ));
       */
    }

    return resultList;
  }

  /**
   * @param uri
   * @return
   */
  public static String getPreferredTitleOfArtefact(String uri) {

    String query = IdiomConstants.SPARQL_PREFIXES + ""
        + "SELECT ?prefTitle WHERE { GRAPH <" + uri + "> { \n" +
        "    ?artefact idiom:preferredTitle ?prefTitle. \n" +
        "  } \n" +
        "} ";

    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();
    QuerySolution processQuery = results.nextSolution();

    return processQuery.get("prefTitle").toString();
  }

  /**
   * @return
   */
  public static List<Artefact> swedishWorkShopQuery() {

    List<Artefact> artefacts = new ArrayList<Artefact>();

    String query = IdiomConstants.SPARQL_PREFIXES +
        "\n\n" +
        "SELECT * WHERE { GRAPH ?graph { \n" +
        "    ?tgObject idiom:preferredTitle ?prefTitle. \n" +
        "	 ?tgObject idiom:identifier ?idiomNumber. \n" +
        "	 ?tgObject idiom:preferredTitle ?title. \n" +
        "	 OPTIONAL {?tgObject idiom:hasMaterialAssignment ?materialRef." +
        "	 BIND(IRI(CONCAT(' + \"<\" + ',?materialRef,' + \">\" + ')) AS ?materialURI)\n" +
        "    GRAPH ?materialURI {" +
        "		?materialRef idiom:assignedMaterial ?material." +
        "	 }} \n" +
        "	 OPTIONAL{?tgObject crm:P41i_was_classified_by ?artefactTypeRef." +
        "	 BIND(IRI(CONCAT(' + \"<\" + ',?artefactTypeRef,' + \">\" + ')) AS ?artefactTypeURI)\n"
        +
        "    GRAPH ?artefactTypeURI {" +
        "		?artefactTypeRef crm:P42_assigned ?artefactType." +
        "	 }} \n" +
        "	 OPTIONAL { ?tgObject idiom:wasFoundAt ?discoveryRef" +
        "	 BIND(IRI(CONCAT(' + \"<\" + ',?discoveryRef,' + \">\" + ')) AS ?discoveryURI)\n" +
        "    GRAPH ?discoveryURI {" +
        "		?discoveryRef crm:P7_took_place_at ?discoveryPlace." +
        "	 }} \n" +
        "OPTIONAL {" +
        "?tgObject idiom:wasDedicatedBy ?dedicationRef. \n" +
        "BIND(IRI(CONCAT(' + \"<\" + ',?dedicationRef,' + \">\" + ')) AS ?dedicationURI)\n" +
        "GRAPH ?dedicationURI {\n" +
        "?dedicationRef dc:date ?dedicationDateRef.\n" +
        "BIND(IRI(CONCAT(' + \"<\" + ',?dedicationDateRef,' + \">\" + ')) AS ?dedicationDateURI)\n"
        +
        "GRAPH ?dedicationDateURI{" +
        "?dedicationDateRef idiom:longcount ?dedicationDate.\n" +
        "?dedicationDateRef idiom:gregorianDate ?dedicationDateGreg.\n" +
        "}}" +
        "}" +
        "  } \n" +
        "}";

    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      if (processQuery.get("tgObject") != null) {
        artefacts.add(buildArtefact(processQuery));
      }
    }

    return artefacts;
  }

  /**
   * @param resultFromSPARQL
   * @return
   */
  public static Artefact buildArtefact(QuerySolution resultFromSPARQL) {

    Artefact artefact = new Artefact();
    artefact.setIdentifier(new Identifier(resultFromSPARQL.get("tgObject").toString()
        .replace("http://textgridrep.de/", "textgrid:")));
    artefact.setIdiomNumber(resultFromSPARQL.get("idiomNumber").toString());
    artefact.setPreferredTitle(resultFromSPARQL.get("title").toString());

    try {
      artefact.setDedicationDate(resultFromSPARQL.get("dedicationDate").toString());
      artefact.setDedicationGregDate(resultFromSPARQL.get("dedicationDateGreg").toString());
    } catch (NullPointerException e) {
      System.out.println("NO DEDICATION");
    }
    try {
      artefact.setMaterial(getVocabulary(resultFromSPARQL.get("material").toString()));
    } catch (NullPointerException e) {
      System.out.println("NO MATERIAL");
    }
    try {
      artefact.setArtefactType(getVocabulary(resultFromSPARQL.get("artefactType").toString()));
    } catch (Exception e) {
      System.out.println("NO ARTEFACTTYPE");
    }

    Place discoveryPlace = new Place();
    try {
      String placeUri = hierarichalPlaceQuery(resultFromSPARQL.get("discoveryPlace").toString());
      try {
        discoveryPlace = getPlaceMetdata(placeUri.replace("http://textgridrep.de/", "textgrid:"));
        artefact.setPlace(discoveryPlace);
      } catch (NullPointerException e) {
        System.out.println("UNKNOWN PLACE");
        Place defaulPlace = new Place();
        defaulPlace.setPlaceName("UNKNOWN");
        defaulPlace.setLongitude("UNKNOWN");
        defaulPlace.setLongitude("UNKNWON");
        artefact.setPlace(defaulPlace);
      }

      artefact.setDiscoveryPlace(
          hierarichalPlaceQuery(resultFromSPARQL.get("discoveryPlace").toString()));
    } catch (NullPointerException e) {
      System.out.println("NO DISCOVERY");
    }

    return artefact;
  }

  /**
   * @param conceptUri
   * @return
   */
  public static String getVocabulary(String conceptUri) {

    String queryInVocabulary = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
        "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
        "PREFIX dct: <http://purl.org/dc/terms/>\n" +
        "\n" +
        "SELECT DISTINCT ?before ?conceptName ?conceptUri\n" +
        "  WHERE {    \n" +
        "   	<" + conceptUri + "> skos:prefLabel ?conceptName.\n" +
        "  FILTER(LANG(?conceptName) = \"\" || LANGMATCHES(LANG(?conceptName), \"en\"))  \n" +
        "}";

    QueryExecution qVoc = QueryExecutionFactory.sparqlService(vocabularyURL, queryInVocabulary);
    ResultSet resultsVoc = qVoc.execSelect();
    QuerySolution solnVoc = resultsVoc.next();

    return solnVoc.get("conceptName").toString().replace("@en", "");
  }

  /**
   * @param textgridURI
   * @return
   */
  public static String hierarichalPlaceQuery(String textgridURI) {

    String query = IdiomConstants.SPARQL_PREFIXES
        + " SELECT  DISTINCT ?placeType ?placeName ?nextPlace ?placeTypeToPrint ?discoveryPlaceRef WHERE { GRAPH <"
        + textgridURI.replace("http://textgridrep.de/", "textgrid:") + "> {        \n" +
        "    	?discoveryPlaceRef rdf:type crm:E53_Place.\n" +
        "        OPTIONAL {\n" +
        "      		?discoveryPlaceRef idiom:placeType ?placeType.\n" +
        "    	}\n" +
        "    	?discoveryPlaceRef crm:P89_falls_within ?nextPlace.\n" +
        "		 ?discoveryPlaceRef crm:P87_is_identified_by ?placeNameRef.  \n" +
        "         BIND(IRI(CONCAT(' + \"<\" + ',?placeNameRef,' + \">\" + ')) AS ?placeNameUrl)\n" +
        "         GRAPH ?placeNameUrl {\n" +
        "                  ?placeNameRef idiom:placeName ?placeName.\n" +
        "                  ?placeNameRef idiom:placeNameType \"preferred\".\n" +
        "         }\n" +
        "  }\n" +
        "}";

    QueryExecution q = QueryExecutionFactory.sparqlService(queryURL, query);
    ResultSet results = q.execSelect();
    List<String> placeTypes = new ArrayList<String>();

    QuerySolution solnNext = null;
    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      if (soln.get("placeType") != null) {
        placeTypes.add(soln.get("placeType").toString());
      }
      solnNext = soln;
    }

    if (solnNext != null && solnNext.get("placeType") != null
        && (placeTypes.contains("http://idiom-projekt.de/voc/placetype/concept000010")
            || placeTypes.contains("http://idiom-projekt.de/voc/placetype/concept000011"))) {
      return solnNext.get("discoveryPlaceRef").toString();
    } else if (solnNext != null
        && !solnNext.get("nextPlace").toString().replace("http://textgridrep.de/", "textgrid:")
            .equals(textgridURI.replace("http://textgridrep.de/", "textgrid:"))) {

      return hierarichalPlaceQuery(solnNext.get("nextPlace").toString());

    } else {
      return null;
    }
  }

  /**
   * @param keyword
   * @return
   */
  public static List<Activity> searchInActivities(String keyword) {

    String query = IdiomConstants.SPARQL_PREFIXES + "SELECT\n" +
        "(group_concat( distinct ?tgObject;separator=\"; \") as ?tgObjects)\n" +
        "(group_concat( distinct ?type;separator=\"; \") as ?types)\n" +
        "(group_concat( distinct ?activityTitle;separator=\"; \") as ?activityTitles)\n" +
        "(group_concat( distinct ?gregorianDate;separator=\"; \") as ?gregorianDates)\n" +
        "(group_concat( distinct ?longcount;separator=\"; \") as ?longcounts)\n" +
        "(group_concat( distinct ?cr;separator=\"; \") as ?crs)" +
        "(group_concat( distinct ?placeName;separator=\"; \") as ?placeNames)\n" +
        "\n" +
        "  WHERE {\n" +
        "    GRAPH ?textgridURI {\n" +
        "          ?tgObject rdf:type crm:E7_Activity;\n" +
        "              idiom:activityTitle ?activityTitle;\n" +
        "				OPTIONAL{?tgObject idiom:activityType ?type}\n" +
        "                OPTIONAL {\n" +
        "                ?timeSpanBegin dc:date ?beginRef.\n" +
        "                  OPTIONAL {\n" +
        // " ?beginRef idiom:isQualifiedBy \"Begin\".\n" +
        "                    ?beginRef idiom:gregorianDate ?gregorianDate.\n" +
        "					 ?beginRef idiom:longcount ?longcount.\n" +
        "					 ?beginRef idiom:tzolkinInput ?tzolkinCoef.\n" +
        "					 ?beginRef idiom:tzolkinUnit ?tzolkinUnit.\n" +
        "					 ?beginRef idiom:haabInput ?haabInput.\n" +
        "					 ?beginRef idiom:haabUnit ?haabUnit.\n" +
        "                  }\n" +
        "            }\n" +
        /// " OPTIONAL {\n" +
        // " ?timeSpanBegin dc:date ?endRef.\n" +
        // " OPTIONAL {\n" +
        // " ?endRef idiom:isQualifiedBy \"End\".\n" +
        // " ?endRef idiom:gregorianDate ?gregorianDateEnd.\n" +
        // " ?endRef idiom:longcount ?endlc." +
        // " }\n" +
        // " }\n" +
        "                  OPTIONAL {\n" +
        "                ?tgObject crm:P7_took_place_at ?placeRef.\n" +
        "                  BIND(IRI(CONCAT(' + \"<\" + ',?placeRef,' + \">\" + ')) AS ?placeURI)\n"
        +
        "                        GRAPH ?placeURI {\n" +
        "                        ?placeRef crm:P87_is_identified_by ?placeNameRef.\n" +
        "                        ?placeNameRef idiom:placeName ?placeName.\n" +
        "        				?placeNameRef idiom:placeNameType ?placeNameType.\n" +
        "                }\n" +
        "            }\n" +
        // " BIND((CONCAT(STR(?gregorianDateBegin), \" - \", STR(?gregorianDateEnd))) AS
        // ?gregorianTimeSpan)\n" +
        // " BIND((CONCAT(STR(?beginlc), \" - \", STR(?endlc))) AS ?longcountTimeSpan)\n" +
        " 			   BIND((CONCAT(STR(?tzolkinCoef), \" \", STR(?tzolkinUnit), \" | \", STR(?haabInput), \" \", STR(?haabUnit))) AS ?cr)\n"
        +
        " FILTER (regex (str(?activityTitle), \"" + keyword
        + "\", \"i\") || regex (str(?longcount), \"" + keyword
        + "\", \"i\") || regex(str(?gregorianDate), \"" + keyword + "\") || regex(str(?cr), \""
        + keyword + "\"))" +
        "\n" +
        "      }\n" +
        "  }GROUP BY ?tgObject";

    // HashMap<String, String> resultList = new HashMap<String, String>();
    List<Activity> activityList = new ArrayList<Activity>();
    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      if (processQuery.get("tgObjects") != null) {
        activityList.add(buildActivity(processQuery));
      }
    }

    return activityList;
  }

  /**
   * @param resultFromSPARQL
   * @return
   */
  private static Activity buildActivity(QuerySolution resultFromSPARQL) {

    Activity activity = new Activity();

    activity.setIdentifier(new Identifier(resultFromSPARQL.get("tgObjects").toString()
        .replace("http://textgridrep.de/", "textgrid:")));
    if (resultFromSPARQL.get("placeNames") != null) {
      activity.setPlaceName(resultFromSPARQL.get("placeNames").toString());
    }

    activity.setTitle(resultFromSPARQL.get("activityTitles").toString());
    activity.setDatesInDifferentFormats(getDateFromQueryResult(resultFromSPARQL));

    return activity;
  }

  /**
   * @param resultFromSPARQL
   * @return
   */
  private static Date getDateFromQueryResult(QuerySolution resultFromSPARQL) {

    Date date = new Date();

    if (resultFromSPARQL.get("longcounts") != null) {
      for (String longcountTimeSpan : Arrays
          .asList(resultFromSPARQL.get("longcounts").toString().split("; "))) {
        date.addLongcountTimeSpan(longcountTimeSpan);
      }
    }
    if (resultFromSPARQL.get("gregorianDates") != null) {
      for (String gregorianTimeSpan : Arrays
          .asList(resultFromSPARQL.get("gregorianDates").toString().split("; "))) {
        date.addGregorianTimeSpan(gregorianTimeSpan);
      }
    }
    if (resultFromSPARQL.get("crs") != null) {
      for (String calenderRoundTimeSpan : Arrays
          .asList(resultFromSPARQL.get("crs").toString().split("; "))) {
        date.addCalenderRoundTimeSpan(calenderRoundTimeSpan);
      }
    }

    return date;
  }

  /**
   * @param keyword
   * @return
   */
  public static List<EpigraphicActor> searchInEpigraphicActors(String keyword) {

    String query = IdiomConstants.SPARQL_PREFIXES +
        "SELECT " +
        "(group_concat( distinct ?actorName;separator=\";\") as ?actorNames)" +
        "(group_concat( distinct ?tgObjects;separator=\";\") as ?tgObjectss)" +
        "(group_concat( distinct ?pndID;separator=\";\") as ?pndIDs)" +
        "(group_concat( distinct ?details;separator=\";\") as ?detailss)" +
        "(group_concat( distinct ?type;separator=\";\") as ?types)" +
        "(group_concat( distinct ?lifedata;separator=\";\") as ?lifedatas)" +
        "(group_concat( distinct ?altActorName;separator=\";\") as ?altActorNames)" +
        "WHERE {" +
        "{" +
        "GRAPH ?textgridURI { " +
        "?tgObjects rdf:type idiom:EpigraphicActor;" +
        "rdf:type ?type." +
        "?tgObjects idiom:prefActorAppellation ?actorAppellation." +
        "?actorAppellation idiom:actorName ?actorName." +
        "?tgObjects idiom:altActorAppellation ?altActorAppellation." +
        "BIND(IRI(CONCAT(\"<\" + ?altActorAppellation + \">\")) AS ?altActorAppellationRef)" +
        "GRAPH ?altActorAppellationRef {" +
        "?altActorAppellation idiom:actorName ?altActorName." +
        "}" +
        " FILTER (regex (str(?actorName), \"" + keyword
        + "\", \"i\") || regex (str(?altActorName), \"" + keyword + "\", \"i\") )" +
        "OPTIONAL {" +
        "?tgObjects idiom:gndID ?details." +
        "}" +
        "          OPTIONAL {" +
        "?tgObjects crm:P98i_was_born ?birthEvent." +
        "?birthEvent dc:date ?birthTimeSpan." +
        "?birthTimeSpan idiom:gregorianDate ?birthDate." +
        "}" +
        "OPTIONAL {" +
        "?tgObjects crm:P100i_died_in ?deathEvent." +
        "?deathEvent dc:date ?deathTimeSpan." +
        "?deathTimeSpan idiom:gregorianDate ?deathDate." +
        "}" +
        "OPTIONAL {?tgObjects schema:gender ?details.}" +
        "bind ( COALESCE(?birthDate, \" ? \") As ?result)" +
        "bind ( COALESCE(?deathDate, \" ? \") As ?result2)" +
        "BIND((CONCAT(STR(?result), \" - \", STR(?result2))) AS ?lifedata)" +
        "}" +
        "}}GROUP BY ?tgObjects";
    List<EpigraphicActor> epigraphicActors = new ArrayList<EpigraphicActor>();
    // HashMap<String, String> resultList = new HashMap<String, String>();
    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      if (processQuery.get("actorNames") != null) {
        epigraphicActors.add(buildEpigaphicActor(processQuery));
      }
      // resultList.put(processQuery.get("tgObjects").toString().replace("http://textgridrep.de/",
      // "textgrid:"), resultDetails);
    }

    return epigraphicActors;
  }

  /**
   * @param queryResult
   * @return
   */
  private static EpigraphicActor buildEpigaphicActor(QuerySolution queryResult) {

    EpigraphicActor epigraphicActor = new EpigraphicActor();

    if (queryResult.get("actorNames") != null) {
      epigraphicActor.setActorName(queryResult.get("actorNames").toString());
    }
    if (queryResult.get("detailss") != null) {
      epigraphicActor.setDetails(queryResult.get("detailss").toString());
    }
    epigraphicActor.setIdentifier(new Identifier(
        queryResult.get("tgObjectss").toString().replace("http://textgridrep.de/", "textgrid:")));

    if (queryResult.get("lifedatas") != null) {
      epigraphicActor.setLifeData(queryResult.get("lifedatas").toString());
    }
    if (queryResult.get("pndIDs") != null) {
      epigraphicActor.setPndID(queryResult.get("pndIDs").toString());
    }
    if (queryResult.get("altActorNames") != null) {
      epigraphicActor.setAltName(queryResult.get("altActorNames").toString());
    }

    return epigraphicActor;
  }

  /**
   * @param keyword
   * @return
   */
  public static List<EpigraphicGroup> searchInEpigraphicGroups(String keyword) {

    String query = IdiomConstants.SPARQL_PREFIXES + "SELECT\n" +
        "(group_concat( distinct ?tgObject;separator=\"; \") as ?tgObjects)\n" +
        "(group_concat( distinct ?groupName;separator=\"; \") as ?groupNames)\n" +
        "(group_concat( distinct ?gndRef;separator=\"; \") as ?gndRefs)\n" +
        "(group_concat( distinct ?groupType;separator=\"; \") as ?groupTypes)\n" +
        "(group_concat( distinct ?residenceTitle;separator=\"; \") as ?residenceTitles)\n" +
        "(group_concat( distinct ?residenceTitleType;separator=\"; \") as ?residenceTitleTypes)\n" +
        "(group_concat( distinct ?groupRef;separator=\"; \") as ?groupRefs)\n" +
        "(group_concat( distinct ?type;separator=\"; \") as ?types)\n" +
        "\n" +
        "        WHERE {\n" +
        "                {\n" +
        "            GRAPH ?textgridURI {\n" +
        "                ?tgObject rdf:type idiom:EpigraphicGroup.\n" +
        "                ?tgObject rdf:type ?type.\n" +
        "                ?tgObject idiom:prefActorAppellation ?nameRef.\n" +
        "                    ?nameRef idiom:actorName ?groupName.\n" +
        "                FILTER (regex (str(?groupName), \"" + keyword + "\", \"i\") )\n" +
        "                OPTIONAL{?tgObject idiom:gndID ?gndRef .}\n" +
        "                OPTIONAL{?tgObject crm:P74_has_current_or_former_residence ?residenceRef .\n"
        +
        "                            BIND(IRI(CONCAT(' + \"<\" + ',?residenceRef,' + \">\" + ')) AS ?residenceUrl)\n"
        +
        "                            GRAPH ?residencUrl {\n" +
        "                                ?residenceRef crm:P87_is_identified_by ?residenceTitleRef .\n"
        +
        "                                ?residenceTitleRef idiom:placeName ?residenceTitle.\n" +
        "                                ?residenceTitleRef idiom:placeNameType ?residenceTitleType .\n"
        +
        "                            }\n" +
        "                }\n" +
        "                         OPTIONAL {\n" +
        "                                ?tgObject crm:P107i_is_former_or_current_member_of ?groupRef .\n"
        +
        "                         }\n" +
        "            }\n" +
        "          }\n" +
        "      } GROUP BY ?tgObject";

    // HashMap<String, String> resultList = new HashMap<String, String>();
    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    List<EpigraphicGroup> epigraphicGroups = new ArrayList<EpigraphicGroup>();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      if (processQuery.get("groupNames") != null) {
        epigraphicGroups.add(buildEpigraphicGroup(processQuery));
      }
    }

    return epigraphicGroups;
  }

  /**
   * @param queryResult
   * @return
   */
  private static EpigraphicGroup buildEpigraphicGroup(QuerySolution queryResult) {

    EpigraphicGroup epigraphicGroup = new EpigraphicGroup();

    epigraphicGroup.setGroupName(queryResult.get("groupNames").toString());
    if (queryResult.get("gndRefs") != null) {
      epigraphicGroup.setGndRef(queryResult.get("gndRefs").toString());
    }
    epigraphicGroup.setIdentifier(new Identifier(
        queryResult.get("tgObjects").toString().replace("http://textgridrep.de/", "textgrid:")));
    if (queryResult.get("residenceTitles") != null) {
      epigraphicGroup.setResidenceTitle(queryResult.get("residenceTitles").toString());
    }

    return epigraphicGroup;
  }

  /**
   * @param uri
   * @return
   */
  public static Place getPlaceMetdata(String uri) {

    Place place = new Place();

    String query = IdiomConstants.SPARQL_PREFIXES + "SELECT\n" +
        "  (group_concat( distinct ?tgObject;separator=\"; \") as ?tgObjects)\n" +
        "  (group_concat( distinct ?placeName;separator=\"; \") as ?placeNames)\n" +
        "  (group_concat( distinct ?placeNameType;separator=\"; \") as ?placeNameTypes)\n" +
        "  (group_concat( distinct ?latitude;separator=\"; \") as ?latitudes)\n" +
        "  (group_concat( distinct ?longitude;separator=\"; \") as ?longitudes)\n" +
        "  (group_concat( distinct ?altitude;separator=\"; \") as ?altitudes)\n" +
        "  (group_concat( distinct ?archeologicalCoordinates;separator=\"; \") as ?archeologicalCoordinatess)\n"
        +
        "  (group_concat( distinct ?locatedPlaceNames;separator=\"; \") as ?locatedPlaceName)\n" +
        "\n" +
        "    WHERE {\n" +
        "      GRAPH <" + uri + "> {\n" +
        "        ?tgObject crm:P87_is_identified_by ?placeNameRef.\n" +
        "	     ?placeNameRef idiom:placeNameType \"preferred\".\n" +
        "        ?placeNameRef idiom:placeName ?placeName.\n" +
        "               OPTIONAL {\n" +
        "                ?tgObject idiom:hasGeoreferencePoint ?geoReferencePointRef.\n" +
        "      OPTIONAL { ?geoReferencePointRef wgs84pos:long ?longitude.}\n" +
        "      OPTIONAL { ?geoReferencePointRef wgs84pos:lat ?latitude.}\n" +
        "      OPTIONAL {?geoReferencePointRef wgs84pos:alt ?altitude. }\n" +
        "                    }\n" +
        "                    OPTIONAL {\n" +
        "                ?tgObject idiom:hasArchaeologicalCoordinates ?archeologicalCoordinatesRef.\n"
        +
        "                  ?archeologicalCoordinatesRef idiom:archaeologicalCoordinates ?archeologicalCoordinates.\n"
        +
        "                  }\n" +
        "            OPTIONAL {\n" +
        "                 ?tgObject crm:P89_falls_within ?locatedWithinRef\n" +
        "                  BIND(IRI(CONCAT(' + \"<\" + ',?locatedWithinRef,' + \">\" + ')) AS ?locatedPlaceURI)\n"
        +
        "                  GRAPH ?locatedPlaceURI {\n" +
        "        			?locatedWithinRef crm:P87_is_identified_by ?locatedWithinPlaeNameRef.\n"
        +
        "                    ?locatedWithinPlaeNameRef idiom:placeName ?locatedPlaceNames.\n" +
        "        			?locatedWithinPlaeNameRef idiom:placeNameType \"preferred\".\n" +
        "                  }\n" +
        "            }\n" +
        "    }\n" +
        "  }";

    // System.out.println(query);

    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      place = buildPlace(processQuery);

    }

    return place;
  }

  /**
   * @param keyword
   * @return
   */
  public static List<Place> searchInPlaces(String keyword) {

    String query = IdiomConstants.SPARQL_PREFIXES + "SELECT\n" +
        "  (group_concat( distinct ?tgObject;separator=\"; \") as ?tgObjects)\n" +
        "  (group_concat( distinct ?placeName;separator=\"; \") as ?placeNames)\n" +
        "  (group_concat( distinct ?placeNameType;separator=\"; \") as ?placeNameTypes)\n" +
        "  (group_concat( distinct ?latitude;separator=\"; \") as ?latitudes)\n" +
        "  (group_concat( distinct ?longitude;separator=\"; \") as ?longitudes)\n" +
        "  (group_concat( distinct ?altitude;separator=\"; \") as ?altitudes)\n" +
        "  (group_concat( distinct ?archeologicalCoordinates;separator=\"; \") as ?archeologicalCoordinatess)\n"
        +
        "  (group_concat( distinct ?locatedPlaceNames;separator=\"; \") as ?locatedPlaceName)\n" +
        "\n" +
        "    WHERE {\n" +
        "      GRAPH ?textgridURI {\n" +
        "        ?tgObject crm:P87_is_identified_by ?placeNameRef.\n" +
        "          ?placeNameRef idiom:placeName ?placeName.\n" +
        "               OPTIONAL {\n" +
        "                ?tgObject idiom:hasGeoreferencePoint ?geoReferencePointRef.\n" +
        "      OPTIONAL { ?geoReferencePointRef wgs84pos:long ?longitude.}\n" +
        "      OPTIONAL { ?geoReferencePointRef wgs84pos:lat ?latitude.}\n" +
        "      OPTIONAL {?geoReferencePointRef wgs84pos:alt ?altitude. }\n" +
        "                    }\n" +
        "                    OPTIONAL {\n" +
        "                ?tgObject idiom:hasArchaeologicalCoordinates ?archeologicalCoordinatesRef.\n"
        +
        "                  ?archeologicalCoordinatesRef idiom:archaeologicalCoordinates ?archeologicalCoordinates.\n"
        +
        "                  }\n" +
        "            OPTIONAL {\n" +
        "                 ?tgObject crm:P89_falls_within ?locatedWithinRef\n" +
        "                  BIND(IRI(CONCAT(' + \"<\" + ',?locatedWithinRef,' + \">\" + ')) AS ?locatedPlaceURI)\n"
        +
        "                  GRAPH ?locatedPlaceURI {\n" +
        "        			?locatedWithinRef crm:P87_is_identified_by ?locatedWithinPlaeNameRef.\n"
        +
        "                    ?locatedWithinPlaeNameRef idiom:placeName ?locatedPlaceNames.\n" +
        "        			?locatedWithinPlaeNameRef idiom:placeNameType \"preferred\".\n" +
        "                  }\n" +
        "            }\n" +
        "    }\n" +
        "  } GROUP BY ?tgObject HAVING ((regex (str(?placeNames), \"" + keyword + "\", \"i\") ))";

    List<Place> places = new ArrayList<Place>();
    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      places.add(buildPlace(processQuery));
    }

    return places;
  }

  /**
   * @param queryResult
   * @return
   */
  private static Place buildPlace(QuerySolution queryResult) {

    Place place = new Place();

    place.setIdentifier(new Identifier(
        queryResult.get("tgObjects").toString().replace("http://textgridrep.de/", "textgrid:")));

    if (queryResult.get("altitudes") != null) {
      place.setAltitude(queryResult.get("altitudes").toString());
    }
    if (queryResult.get("archeologicalCoordinatess") != null) {
      place.setArcheologicalCoordinates(queryResult.get("archeologicalCoordinatess").toString());
    }
    if (queryResult.get("latitudes") != null) {
      place.setLatitude(queryResult.get("latitudes").toString());
    }
    if (queryResult.get("locatedPlaceName") != null) {
      place.setLocatedPlaceNames(queryResult.get("locatedPlaceName").toString());
    }
    if (queryResult.get("longitudes") != null) {
      place.setLongitude(queryResult.get("longitudes").toString());
    }

    place.setPlaceName(queryResult.get("placeNames").toString());

    return place;
  }

  /**
   * @param keyword
   * @return
   */
  public static List<Dedication> searchInDedication(String keyword) {

    String query = IdiomConstants.SPARQL_PREFIXES
        + "SELECT ?dedication ?title ?placeName ?placeNameType ?dateRef ?beginlcs\n" +
        "  {\n" +
        "    GRAPH ?tgURI {\n" +
        "    ?dedication rdf:type idiom:Dedication.\n" +
        "    ?dedication idiom:activityTitle ?title.\n" +
        "    FILTER (regex (str(?title), \"" + keyword + "\", \"i\") )\n" +
        "    OPTIONAL{\n" +
        "      ?dedication idiom:commissioner ?commissioner.\n" +
        "    }\n" +
        "    OPTIONAL{\n" +
        "      ?dedication crm:P7_took_place_at ?placeRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?placeRef,' + \">\" + ')) AS ?placeURI)\n" +
        "      	GRAPH ?placeURI {\n" +
        "        	?placeRef crm:P87_is_identified_by ?placeNameRef.\n" +
        "            ?placeNameRef idiom:placeName ?placeName.\n" +
        "        	?placeNameRef idiom:placeNameType ?placeNameType.\n" +
        "        }\n" +
        "    }\n" +
        "    OPTIONAL{\n" +
        "      ?dedication dc:date ?dateRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?dateRef,' + \">\" + ')) AS ?dateURI)\n" +
        "      	GRAPH ?dateUri {\n" +
        "      		?dateRef idiom:longcount ?beginlcs.\n" +
        "    	}\n" +
        "\n" +
        "    }\n" +
        "  }\n" +
        "}";

    List<Dedication> dedications = new ArrayList<Dedication>();
    HashMap<String, String> resultList = new HashMap<String, String>();
    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      dedications.add(buildDedication(processQuery));
      String title = "";

      if (processQuery.get("title") != null) {
        title = processQuery.get("title").toString() + ", ";
      } else {
        title = "";
      }

      String placeName;
      if (processQuery.get("placeName") != null) {
        placeName = processQuery.get("placeName").toString();
      } else {
        placeName = "";
      }
      String placeNameType;

      if (processQuery.get("placeNameType") != null) {
        placeNameType = processQuery.get("placeNameType").toString() + ", ";
      } else {
        placeNameType = "";
      }

      String longcount;
      if (processQuery.get("longcount") != null) {
        longcount = processQuery.get("longcount").toString() + ", ";
      } else {
        longcount = "";
      }

      String resultDetails = title + placeName + placeNameType + longcount;

      resultList.put(
          processQuery.get("dedication").toString().replace("http://textgridrep.de/", "textgrid:"),
          resultDetails);
    }

    return dedications;
  }

  /**
   * @param queryResult
   * @return
   */
  private static Dedication buildDedication(QuerySolution queryResult) {

    Dedication dedication = new Dedication();
    dedication.setTitle(queryResult.get("title").toString());
    if (queryResult.get("placeName") != null) {
      dedication.setPlaceName(queryResult.get("placeName").toString());
    }
    dedication.setIdentifier(new Identifier(
        queryResult.get("dedication").toString().replace("http://textgridrep.de/", "textgrid:")));
    dedication.setDatesInDifferentFormats(getDateFromQueryResult(queryResult));

    return dedication;
  }

  /**
   * @param textgridURI
   * @return
   */
  public static String getImageOfGraph(String textgridURI) {

    String imageOfGraph;

    String query = IdiomConstants.SPARQL_PREFIXES + "SELECT ?graph ?tgURI ?image WHERE { \n" +
        "  GRAPH <" + textgridURI + "> { \n" +
        "    ?graph rdf:type idiomcat:Graph. \n" +
        "    ?graph idiomcat:hasDigitalImage ?image.\n" +
        "  }\n" +
        "} ";
    QueryUtillities queryUtilities = new QueryUtillities();
    imageOfGraph = queryUtilities.queryExecution(QueryUtillities.executeQuery(query), "image");

    return imageOfGraph;
  }

}
