package org.classicmayan.tools;

/**
 *
 */
public class EpigraphicActor {

  private Identifier identifier;
  private String actorName;
  private String pndID;
  private String details;
  private String lifeData;
  private String altName;

  /**
   * @return
   */
  public Identifier getIdentifier() {
    return this.identifier;
  }

  /**
   * @return
   */
  public String getAltName() {
    return this.altName;
  }

  /**
   * @param altName
   */
  public void setAltName(String altName) {
    this.altName = altName;
  }

  /**
   * @return
   */
  public String getLifeData() {
    return this.lifeData;
  }

  /**
   * @param lifeData
   */
  public void setLifeData(String lifeData) {
    this.lifeData = lifeData;
  }

  /**
   * @return
   */
  public String getDetails() {
    return this.details;
  }

  /**
   * @param details
   */
  public void setDetails(String details) {
    this.details = details;
  }

  /**
   * @return
   */
  public String getPndID() {
    return this.pndID;
  }

  /**
   * @param pndID
   */
  public void setPndID(String pndID) {
    this.pndID = pndID;
  }

  /**
   * @return
   */
  public String getActorName() {
    return this.actorName;
  }

  /**
   * @param actorName
   */
  public void setActorName(String actorName) {
    this.actorName = actorName;
  }

  /**
   * @param identifier
   */
  public void setIdentifier(Identifier identifier) {
    this.identifier = identifier;
  }

}
