/**
 * This software is copyright (c) 2023 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public final class IdiomConstants {

  // **
  // CHANGE PROPERTIES FILE BELOW!
  // **

  // public static final String PROPERTIES_FILE =
  // "src/main/resources/idiomQueries-SECRET-dev.properties";
  public static final String PROPERTIES_FILE =
      "src/main/resources/idiomQueries-SECRET-prod.properties";

  // **
  // CHANGE PROPERTIES FILE ABOVE!
  // **

  public static final String DEFAULT_TGURL = "https://textgridlab.org/1.0/";
  public static final String TGURL_PROPERTY = "tg.url";
  public static final String SCOPE_PROPERTY = "scope";
  public static final String SCOPE_IMPORT_ALL = "importAll";
  public static final String SCOPE_UPDATE = "update";
  public static final String SCOPE_IMPORT_NEW = "importNew";
  public static final String SCOPE_IMPORT_MISSING = "importMissing";
  public static final String SCOPE_UPDATE_AND_IMPORT_NEW = "updateAndImportNew";
  public static final String DRY_RUN_PROPERTY = "dryRun";
  public static final String SID_PROPERTY = "tg.sessionID";
  public static final String PID_PROPERTY = "tg.projectID";
  public static final String FORMAT_TO_FILTER_PROPERTY = "tg.formatToFilter";
  public static final String QUERY_SIZE_PROPERTY = "tg.querySize";
  public static final String TGCRUD_ENDPOINT_PROPERTY = "tg.crudEndpoint";
  public static final String TGSEARCH_ENDPOINT_PROPERTY = "tg.searchEndpoint";
  public static final String MEDIUM_COLLECTION_PROPERTY =
      "tg.conedaKorMediumMetadataCollectionURI";
  public static final String MAX_COLLECTION_SIZE_PROPERTY = "tg.maxConedaKorCollectionSize";
  public static final String DATE_PROPERTY = "tg.date";

  /**
   * String for addresses of the SPARQL endpoints for the metadata and also for the vocabulary.
   */
  public static final String SPARQL_METADATA_ENDPOINT =
      "https://www.classicmayan.org/trip/metadata/query";
  public static final String SPARQL_VOCABULARY_ENDPOINT =
      "https://www.classicmayan.org/trip/vocabularies/query";

  /**
   * Strings for ALL possible and sometimes required prefixes for the SPARQL endpoints
   */
  public static final String SPARQL_PREFIXES =
      "prefix dct: <http://purl.org/dc/terms#>\n" +
          "prefix dc: <http://purl.org/dc/elements/1.1/>\n" +
          "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
          "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
          "prefix owl: <http://www.w3.org/2002/07/owl#>\n" +
          "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
          "prefix idi: <http://idiom.uni-bonn.de/terms#>\n" +
          "prefix idiom: <http://idiom-projekt.de/schema/>\n" +
          "prefix ecrm: <http://erlangen-crm.org/current#>\n" +
          "prefix edm: <http://www.europeana.eu/schemas/edm#>\n" +
          "prefix form: <http://rdd.sub.uni-goettingen.de/rdfform#>\n" +
          "prefix crm: <http://erlangen-crm.org/current/>\n" +
          "prefix tgforms: <http://www.tgforms.de/terms#>\n" +
          "prefix textgrid: <http://textgridrep.de/>\n" +
          "prefix schema: <http://schema.org/>\n" +
          "prefix skos: <http://www.w3.org/2004/02/skos/core#>\n" +
          "prefix wgs84pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n" +
          "prefix rel: <http://purl.org/vocab/relationship/>\n" +
          "prefix edm: <http://www.europeana.eu/schemas/edm/>\n" +
          "prefix idiomcat: <http://idiom-projekt.de/catalogue/>";

  /**
   * RDF predicated for the linguistic analysis.
   */
  public static final String SIGN_LOGOGRAPHIC_READING_REF = "idiomcat:hasLogographicReading";
  public static final String SIGN_CONFIDENCE_LEVEL_LOGOGRAPHIC_READING = "idiomcat:hasCLLR";
  public static final String SIGN_LOGOGRAPHIC_MEANING_REF = "idiomcat:hasLogographicMeaning";
  public static final String SIGN_CONFIDENCE_LEVEL_LOGOGRAPHIC_MEANING = "idiomcat:hasCLLM";
  public static final String SIGN_SYLLABIC_READING_REF = "idiomcat:hasSyllabicReading";
  public static final String SIGN_CONFIDENCE_LEVEL_SYLLABIC_READING = "idiomcat:hasCLSR";
  public static final String SIGN_DIACRITIC_FUNCTION_REF = "idiomcat:hasDiacriticFunction";
  public static final String SIGN_CONFIDENCE_LEVEL_DIACRITIC_FUNCTION = "idiomcat:hasCLDF";
  public static final String SIGN_NUMERIC_FUNCTION_REF = "idiomcat:hasNumericFunction";
  public static final String SIGN_CONFIDENCE_LEVEL_NUMERIC_FUNCTION = "idiomcat:hasCLNF";
  public static final String SIGN_LOGOGRAPHIC_READING =
      "    OPTIONAL{ ?textgridURI " + IdiomConstants.SIGN_LOGOGRAPHIC_READING_REF
          + " ?linguisticAnalysisRef.\n" +
          "    BIND(IRI(CONCAT(' + \"<\" + ',?linguisticAnalysisRef,' + \">\" + ')) AS ?linguisticAnalysisURI)\n"
          +
          "    GRAPH ?linguisticAnalysisURI {\n" +
          "      ?linguisticAnalysisRef idiomcat:transliterationValue ?logographic_reading_value.\n"
          +
          "      ?linguisticAnalysisRef " + IdiomConstants.SIGN_CONFIDENCE_LEVEL_LOGOGRAPHIC_READING
          + " ?clRef.\n" +
          "      BIND(IRI(CONCAT(' + \"<\" + ',?clRef,' + \">\" + ')) AS ?clURI)\n" +
          "      GRAPH ?clURI {\n" +
          "        ?clRef idiomcat:resultsInLevel ?logographic_reading_level.\n" +
          "      }\n" +
          "BIND((CONCAT(STR(?logographic_reading_value), \" : \", STR(?logographic_reading_level) )) AS ?logographic_reading)\n"
          +
          "    }}\n";
  public static final String SIGN_LOGOGRAPHIC_MEANING =
      "    OPTIONAL { ?textgridURI idiomcat:hasLogographicMeaning ?logographic_meaning_Ref.\n"
          + "    BIND(IRI(CONCAT(' + \"<\" + ',?logographic_meaning_Ref,' + \">\" + ')) AS ?logographic_meaning_URI)\n"
          + "    GRAPH ?logographic_meaning_URI {\n"
          + "      ?logographic_meaning_Ref idiomcat:transliterationValue ?logographic_meaning_value.\n"
          + "      ?logographic_meaning_Ref idiomcat:hasCLLM ?lmclRef.\n"
          + "      BIND(IRI(CONCAT(' + \"<\" + ',?lmclRef,' + \">\" + ')) AS ?lmclURI)\n"
          + "      GRAPH ?lmclURI {\n"
          + "        ?lmclRef idiomcat:resultsInLevel ?logographic_meaning_level.\n"
          + "      }\n"
          + "        BIND((CONCAT(STR(?logographic_meaning_value), \" : \", STR(?logographic_meaning_level) )) AS ?logographic_meaning)\n"
          + "    }}";
  public static final String SIGN_SYLLABIC_READING =
      "OPTIONAL{  ?textgridURI idiomcat:hasSyllabicReading ?syllabic_reading_Ref.\n"
          + "	BIND(IRI(CONCAT(' + \"<\" + ',?syllabic_reading_Ref,' + \">\" + ')) AS ?syllabic_reading_URI)\n"
          + " 	GRAPH ?syllabic_reading_URI {\n"
          + " 	      	?syllabic_reading_Ref idiomcat:transliterationValue ?syllabic_reading_value.\n"
          + " 		  	?syllabic_reading_Ref idiomcat:hasCLSR ?srclRef.\n"
          + " 			BIND(IRI(CONCAT(' + \"<\" + ',?srclRef,' + \">\" + ')) AS ?srclURI)\n"
          + " 			GRAPH ?srclURI {\n"
          + " 				?srclRef idiomcat:resultsInLevel ?syllabic_reading_level.\n"
          + " 			} \n"
          + "        	BIND((CONCAT(STR(?syllabic_reading_value), \" : \", STR(?syllabic_reading_level) )) AS ?syllabic_reading)\n"
          + " 	}\n"
          + "}";
  public static final String SIGN_DIACRITIC_FUNCTION =
      "OPTIONAL{ ?textgridURI idiomcat:hasDiacriticFunction ?diacritic_function_Ref.\n" +
          "    BIND(IRI(CONCAT(' + \"<\" + ',?diacritic_function_Ref,' + \">\" + ')) AS ?diacritic_function_URI)\n"
          +
          "    GRAPH ?diacritic_function_URI {\n" +
          "      ?diacritic_function_Ref idiomcat:transliterationValue ?diacritic_function_value.\n"
          +
          "      ?diacritic_function_Ref idiomcat:hasCLDF ?dfclRef.\n" +
          "      BIND(IRI(CONCAT(' + \"<\" + ',?dfclRef,' + \">\" + ')) AS ?dfclURI)\n" +
          "      GRAPH ?dfclURI {\n" +
          "        ?dfclRef idiomcat:resultsInLevel ?diacritic_function_level.\n" +
          "      }\n" +
          "     BIND((CONCAT(STR(?diacritic_function_value), \" : \", STR(?diacritic_function_level) )) AS ?diacritic_function)\n"
          +
          "    }\n" +
          "}";
  public static final String SIGN_NUMERIC_FUNCTION =
      "OPTIONAL{ ?textgridURI idiomcat:hasNumericFunction ?numeric_function_Ref.\n" +
          "    BIND(IRI(CONCAT(' + \"<\" + ',?numeric_function_Ref,' + \">\" + ')) AS ?numeric_function_URI)\n"
          +
          "    GRAPH ?numeric_function_URI {\n" +
          "      ?numeric_function_Ref idiomcat:transliterationValue ?numeric_function_value.\n" +
          "      ?numeric_function_Ref idiomcat:hasCLNF ?nfclRef.\n" +
          "      BIND(IRI(CONCAT(' + \"<\" + ',?nfclRef,' + \">\" + ')) AS ?nfclURI)\n" +
          "      GRAPH ?nfclURI {\n" +
          "        ?nfclRef idiomcat:resultsInLevel ?numeric_function_level.\n" +
          "      }\n" +
          "     BIND((CONCAT(STR(?numeric_function_value), \" : \", STR(?numeric_function_level) )) AS ?numeric_function)\n"
          +
          "    }\n" +
          "}";
  public static final String SOURCE =
      "OPTIONAL{ ?textgridURI dct:isReferencedBy ?source_ref.\n"
          + "BIND(IRI(CONCAT(' + \"<\" + ',?source_ref,' + \">\" + ')) AS ?source_URI)\n"
          + "    GRAPH ?source_URI {\n"
          + "      ?source_ref idiom:zoteroURI ?zoterouri.\n"
          + "      ?source_ref dct:bibliographicCitation ?bibliographicCitation.\n"
          + "      ?source_ref schema:pagination ?page.\n"
          + "      ?source_ref idiom:attributionType ?attributionType.\n"
          + "      ?source_ref idiom:hasConfidenceLevel ?confidenceLevel.\n"
          + "    }\n"
          + "    BIND((CONCAT(STR(?zoterouri), \" % \", STR(?bibliographicCitation), \" % \", STR(?page), \" % \", STR(?attributionType), \" % \", STR(?confidenceLevel))) AS ?source)    \n"
          + " }";

}
