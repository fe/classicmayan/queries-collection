/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 */
public class ZoteroQueries {

  private String host = "https://api.zotero.org/";
  private String userNumber = "users/1757564/";
  private String searchOrder = "items";
  private String sortAndFormat = "?sort=date&limit=40&format=json";
  private String apiKey = "";
  private String apiQueryTag = "&q=Grube";
  private String authors = "";
  private String editors = "";

  /**
   * 
   */
  public ZoteroQueries() {

  }

  /**
   * @throws IOException
   */
  public void connectToZoteroDB() throws IOException {

    this.host = this.host + this.userNumber + this.searchOrder + this.sortAndFormat + this.apiKey
        + this.apiQueryTag;

    System.out.println(this.host);

    HttpClient httpClient = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(this.host + this.userNumber + this.searchOrder
        + this.sortAndFormat + this.apiKey + this.apiQueryTag);

    try {
      HttpResponse response = httpClient.execute(request);
      HttpEntity entity = response.getEntity();

      // Read the contents of an entity and return it as a String.
      String content = EntityUtils.toString(entity);

      // TODO Why we only print this here?
      System.out.println(content);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * @param rd
   * @return
   * @throws IOException
   */
  private static String readAll(Reader rd) throws IOException {

    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }

    return sb.toString();
  }

  /**
   * @param url
   * @return
   * @throws IOException
   * @throws JSONException
   */
  public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {

    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONArray json = new JSONArray(jsonText);

      return json;

    } finally {
      is.close();
    }
  }

  /**
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public List<String> getCitations() throws JSONException, IOException {

    List<String> citations = new ArrayList<String>();

    JSONArray resultsFromZoteroSearch = readJsonFromUrl(
        "https://api.zotero.org/users/1757564/items?sort=date&limit=40&format=json&key="
            + this.getApiKey() + "&q=Tikal");

    for (int i = 0; i < resultsFromZoteroSearch.length(); i++) {
      JSONObject jObject = resultsFromZoteroSearch.getJSONObject(i);
      String itemType = jObject.getJSONObject("data").getString("itemType");

      String citation = "";
      extractAuthors(jObject);

      citation = this.getAuthors() + "(" + jObject.getJSONObject("data").getString("date") + "): "
          + jObject.getJSONObject("data").getString("title") + ". ";

      if (itemType.equals("journalArticle")) {
        System.out.println(getCitationForJournalArticle(jObject));
      }
      if (itemType.equals("blogPost")) {
        System.out.println(getCitationForBlogPost(jObject));
      }
      if (itemType.equals("encyclopediaArticle") || itemType.equals("webpage")) {
        System.out.println(getCitationForEncyclopediaArticleOrWebpage(jObject));
      }
      if (itemType.equals("book")) {
        System.out.println(getCitationForBook(jObject));
      }
      if (itemType.equals("bookSection")) {
        System.out.println(citation.concat(getCitationForBookSection(jObject)));
      }
      if (itemType.equals("manuscript")) {
        System.out.println(getCitationForManuscript(jObject));
      }
      if (itemType.equals("thesis")) {
        System.out.println(getCitationForThesis(jObject));
      }
      if (itemType.equals("magazineArticle")) {
        System.out.println(getCitationForMagazineArticle(jObject));
      }
      if (itemType.equals("newspaperArticle")) {
        System.out.println(getCitationForNewspaperArticle(jObject));
      }
      if (itemType.equals("report")) {
        System.out.println(getCitationForReport(jObject));
      }
      if (itemType.equals("document")) {
        System.out.println(getCitationForDocument(jObject));
      }
      if (itemType.equals("map")) {
        System.out.println(getCitationForMap(jObject));
      }
      if (itemType.equals("presentation")) {
        System.out.println(getCitationForPresentation(jObject));
      }

      System.out.println("\n\n");
    }

    return citations;
  }

  /**
   * @param zoteroKey
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public String getCitationByKey(String zoteroKey) throws JSONException, IOException {

    JSONArray zoteroObjectIdentifiedByKes =
        readJsonFromUrl("https://api.zotero.org/users/1757564/items?itemKey=" + zoteroKey);

    JSONObject jObject = zoteroObjectIdentifiedByKes.getJSONObject(0);
    String itemType = jObject.getJSONObject("data").getString("itemType");
    String citation;
    extractAuthors(jObject);

    citation = getAuthors() + "(" + jObject.getJSONObject("data").getString("date") + "): "
        + jObject.getJSONObject("data").getString("title") + ". ";

    if (itemType.equals("journalArticle")) {
      System.out.println(getCitationForJournalArticle(jObject));
    }
    if (itemType.equals("blogPost")) {
      System.out.println(getCitationForBlogPost(jObject));
    }
    if (itemType.equals("encyclopediaArticle") || itemType.equals("webpage")) {
      System.out.println(getCitationForEncyclopediaArticleOrWebpage(jObject));
    }
    if (itemType.equals("book")) {
      System.out.println(getCitationForBook(jObject));
    }
    if (itemType.equals("bookSection")) {
      citation = citation.concat(getCitationForBookSection(jObject));
    }
    if (itemType.equals("manuscript")) {
      System.out.println(getCitationForManuscript(jObject));
    }
    if (itemType.equals("thesis")) {
      System.out.println(getCitationForThesis(jObject));
    }
    if (itemType.equals("magazineArticle")) {
      System.out.println(getCitationForMagazineArticle(jObject));
    }
    if (itemType.equals("newspaperArticle")) {
      System.out.println(getCitationForNewspaperArticle(jObject));
    }
    if (itemType.equals("report")) {
      System.out.println(getCitationForReport(jObject));
    }
    if (itemType.equals("document")) {
      System.out.println(getCitationForDocument(jObject));
    }
    if (itemType.equals("map")) {
      System.out.println(getCitationForMap(jObject));
    }
    if (itemType.equals("presentation")) {
      System.out.println(getCitationForPresentation(jObject));
    }

    return citation;
  }

  /**
   * @param itemForAuthors
   * @return
   */
  public String extractAuthors(JSONObject itemForAuthors) {

    JSONArray authorsInJson = itemForAuthors.getJSONObject("data").getJSONArray("creators");
    List<String> authorsList = new ArrayList<String>();
    for (int i = 0; i < authorsInJson.length(); i++) {
      if (authorsInJson.getJSONObject(i).get("creatorType").equals("author")) {
        if (i == 0) {
          authorsList.add(authorsInJson.getJSONObject(i).get("lastName") + ", "
              + authorsInJson.getJSONObject(i).get("firstName"));
        } else {
          authorsList.add(authorsInJson.getJSONObject(i).get("firstName") + " "
              + authorsInJson.getJSONObject(i).get("lastName"));
        }
      }
    }

    int authorCount = 0;
    for (String author : authorsList) {
      if (authorCount < authorsList.size() - 1) {
        setAuthors(getAuthors().concat(author) + ", ");
      } else {
        setAuthors(getAuthors().concat(author) + " ");
      }
      authorCount++;
    }

    return getAuthors();
  }

  /**
   * @param itemForEditors
   * @return
   */
  public String extractEditors(JSONObject itemForEditors) {

    JSONArray editorsInJson = itemForEditors.getJSONObject("data").getJSONArray("creators");
    for (int i = 0; i < editorsInJson.length(); i++) {
      if (editorsInJson.getJSONObject(i).get("creatorType").equals("editor")) {
        setEditors(getEditors().concat(editorsInJson.getJSONObject(i).get("firstName") + " "
            + editorsInJson.getJSONObject(i).get("lastName")) + ", ");
      }
    }

    return getEditors();
  }


  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForJournalArticle(JSONObject itemFromZoteroRequest) {

    String publicationTitle;
    String volume;
    String pages;

    if (itemFromZoteroRequest.getJSONObject("data").getString("publicationTitle").length() > 0) {
      publicationTitle =
          itemFromZoteroRequest.getJSONObject("data").getString("publicationTitle") + " ";
    } else {
      publicationTitle = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("volume").length() > 0) {
      volume = itemFromZoteroRequest.getJSONObject("data").getString("volume") + " ";
    } else {
      volume = "";
    }
    if (itemFromZoteroRequest.getJSONObject("data").getString("pages").length() > 0) {
      pages = itemFromZoteroRequest.getJSONObject("data").getString("pages");
    } else {
      pages = "";
    }

    return publicationTitle + volume + pages;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForBlogPost(JSONObject itemFromZoteroRequest) {

    String blogTitle;
    String url;
    String dataAccess;

    if (itemFromZoteroRequest.getJSONObject("data").getString("blogTitle").length() > 0) {
      blogTitle = " Electronic Document. "
          + itemFromZoteroRequest.getJSONObject("data").getString("blogTitle") + " ";
    } else {
      blogTitle = "";
    }
    if (itemFromZoteroRequest.getJSONObject("data").getString("url").length() > 0) {
      url = itemFromZoteroRequest.getJSONObject("data").getString("url") + " ";
    } else {
      url = "";
    }
    if (itemFromZoteroRequest.getJSONObject("data").getString("dateAdded").length() > 0) {
      dataAccess = itemFromZoteroRequest.getJSONObject("data").getString("dateAdded");
    } else {
      dataAccess = "";
    }

    return blogTitle + url + dataAccess;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForEncyclopediaArticleOrWebpage(
      JSONObject itemFromZoteroRequest) {

    String url;
    String dataAccess;

    if (itemFromZoteroRequest.getJSONObject("data").getString("url").length() > 0) {
      url = itemFromZoteroRequest.getJSONObject("data").getString("url") + " ";
    } else {
      url = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("dateAdded").length() > 0) {
      dataAccess = itemFromZoteroRequest.getJSONObject("data").getString("url");
    } else {
      dataAccess = "";
    }

    return " Electronic Document. " + url + dataAccess;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForBook(JSONObject itemFromZoteroRequest) {

    String publisher;
    String place;
    String seriesNumber;
    String series;
    String volume;

    if (itemFromZoteroRequest.getJSONObject("data").getString("publisher").length() > 0) {
      publisher = itemFromZoteroRequest.getJSONObject("data").getString("publisher") + ", ";
    } else {
      publisher = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("place").length() > 0) {
      place = itemFromZoteroRequest.getJSONObject("data").getString("place");
    } else {
      place = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("seriesNumber").length() > 0) {
      seriesNumber =
          " " + itemFromZoteroRequest.getJSONObject("data").getString("seriesNumber") + ". ";
    } else {
      seriesNumber = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("series").length() > 0) {
      series = itemFromZoteroRequest.getJSONObject("data").getString("series");
    } else {
      series = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("volume").length() > 0) {
      volume = ". Vol. " + itemFromZoteroRequest.getJSONObject("data").getString("volume") + ". ";
    } else {
      volume = "";
    }

    return series + seriesNumber + volume + publisher + place;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private String getCitationForBookSection(JSONObject itemFromZoteroRequest) {

    extractEditors(itemFromZoteroRequest);

    String editorsList;
    String bookTitle;
    String volume;
    String pages;
    String series;
    String seriesNumber;
    String publisher;
    String place;

    if (getEditors().length() > 0) {
      editorsList = ", edited by: " + extractEditors(itemFromZoteroRequest);
    } else {
      editorsList = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("bookTitle").length() > 0) {
      bookTitle = "In: " + itemFromZoteroRequest.getJSONObject("data").getString("bookTitle");
    } else {
      bookTitle = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("volume").length() > 0) {
      volume = ". Vol. " + itemFromZoteroRequest.getJSONObject("data").getString("volume") + ". ";
    } else {
      volume = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("pages").length() > 0) {
      pages = "pp. " + itemFromZoteroRequest.getJSONObject("data").getString("pages") + ". ";
    } else {
      pages = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("series").length() > 0) {
      series = itemFromZoteroRequest.getJSONObject("data").getString("series");
    } else {
      series = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("seriesNumber").length() > 0) {
      seriesNumber =
          " " + itemFromZoteroRequest.getJSONObject("data").getString("seriesNumber") + ". ";
    } else {
      seriesNumber = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("publisher").length() > 0) {
      publisher = itemFromZoteroRequest.getJSONObject("data").getString("publisher") + ", ";
    } else {
      publisher = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("place").length() > 0) {
      place = itemFromZoteroRequest.getJSONObject("data").getString("place");
    } else {
      place = "";
    }

    return bookTitle + volume + editorsList + pages + series + seriesNumber + publisher + place;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForManuscript(JSONObject itemFromZoteroRequest) {

    String manuscriptType;
    String callNumber;
    String archive;

    if (itemFromZoteroRequest.getJSONObject("data").getString("manuscriptType").length() > 0) {
      manuscriptType =
          itemFromZoteroRequest.getJSONObject("data").getString("manuscriptType") + ". ";
    } else {
      manuscriptType = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("callNumber").length() > 0) {
      callNumber = itemFromZoteroRequest.getJSONObject("data").getString("callNumber") + ".";
    } else {
      callNumber = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("archive").length() > 0) {
      archive = itemFromZoteroRequest.getJSONObject("data").getString("archive") + ". ";
    } else {
      archive = "";
    }

    return manuscriptType + callNumber + archive;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForThesis(JSONObject itemFromZoteroRequest) {

    String thesisType;
    String place;
    String university;
    String url;

    if (itemFromZoteroRequest.getJSONObject("data").getString("thesisType").length() > 0) {
      thesisType = itemFromZoteroRequest.getJSONObject("data").getString("thesisType") + ", ";
    } else {
      thesisType = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("place").length() > 0) {
      place = itemFromZoteroRequest.getJSONObject("data").getString("place") + ", ";
    } else {
      place = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("university").length() > 0) {
      university = itemFromZoteroRequest.getJSONObject("data").getString("university") + ". ";
    } else {
      university = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("url").length() > 0) {
      url = itemFromZoteroRequest.getJSONObject("data").getString("url");
    } else {
      url = "";
    }

    return thesisType + place + university + url;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForMagazineArticle(JSONObject itemFromZoteroRequest) {

    String publicationTitle;
    String volume;
    String pages;

    if (itemFromZoteroRequest.getJSONObject("data").getString("publicationTitle").length() > 0) {
      publicationTitle =
          itemFromZoteroRequest.getJSONObject("data").getString("publicationTitle") + " ";
    } else {
      publicationTitle = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("volume").length() > 0) {
      volume = itemFromZoteroRequest.getJSONObject("data").getString("volume") + ": ";
    } else {
      volume = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("pages").length() > 0) {
      pages = itemFromZoteroRequest.getJSONObject("data").getString("pages") + ".";
    } else {
      pages = "";
    }

    return publicationTitle + volume + pages;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForNewspaperArticle(JSONObject itemFromZoteroRequest) {

    String publicationTitle;
    String section;
    String pages;

    if (itemFromZoteroRequest.getJSONObject("data").getString("publicationTitle").length() > 0) {
      publicationTitle =
          itemFromZoteroRequest.getJSONObject("data").getString("publicationTitle") + " ";
    } else {
      publicationTitle = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("section").length() > 0) {
      section = itemFromZoteroRequest.getJSONObject("data").getString("section") + ": ";
    } else {
      section = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("pages").length() > 0) {
      pages = itemFromZoteroRequest.getJSONObject("data").getString("pages") + " ";
    } else {
      pages = "";
    }

    return publicationTitle + section + pages;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForReport(JSONObject itemFromZoteroRequest) {

    String reportType;
    String institution;
    String place;
    String url;

    if (itemFromZoteroRequest.getJSONObject("data").getString("reportType").length() > 0) {
      reportType = itemFromZoteroRequest.getJSONObject("data").getString("reportType") + ". ";
    } else {
      reportType = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("institution").length() > 0) {
      institution = itemFromZoteroRequest.getJSONObject("data").getString("institution") + ", ";
    } else {
      institution = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("place").length() > 0) {
      place = itemFromZoteroRequest.getJSONObject("data").getString("place") + ". ";
    } else {
      place = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("url").length() > 0) {
      url = itemFromZoteroRequest.getJSONObject("data").getString("url") + ". ["
          + itemFromZoteroRequest.getJSONObject("data").getString("dateAdded") + "]";
    } else {
      url = "";
    }

    return reportType + institution + place + url;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForDocument(JSONObject itemFromZoteroRequest) {

    String url;

    if (itemFromZoteroRequest.getJSONObject("data").getString("url").length() > 0) {
      url = itemFromZoteroRequest.getJSONObject("data").getString("url") + ". ["
          + itemFromZoteroRequest.getJSONObject("data").getString("dateAdded") + "]";
    } else {
      url = "";
    }

    return url;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForMap(JSONObject itemFromZoteroRequest) {

    String place;
    String url;
    String publisher;

    if (itemFromZoteroRequest.getJSONObject("data").getString("url").length() > 0) {
      url = itemFromZoteroRequest.getJSONObject("data").getString("url") + ". ["
          + itemFromZoteroRequest.getJSONObject("data").getString("dateAdded") + "]";
    } else {
      url = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("place").length() > 0) {
      place = itemFromZoteroRequest.getJSONObject("data").getString("place") + ". ";
    } else {
      place = "";
    }

    if (itemFromZoteroRequest.getJSONObject("data").getString("publisher").length() > 0) {
      publisher = itemFromZoteroRequest.getJSONObject("data").getString("publisher") + ", ";
    } else {
      publisher = "";
    }

    return publisher + place + url;
  }

  /**
   * @param itemFromZoteroRequest
   * @return
   */
  private static String getCitationForPresentation(JSONObject itemFromZoteroRequest) {

    String meetingName;

    if (itemFromZoteroRequest.getJSONObject("data").getString("meetingName").length() > 0) {
      meetingName = itemFromZoteroRequest.getJSONObject("data").getString("meetingName") + ", ";
    } else {
      meetingName = "";
    }

    return meetingName;
  }

  /**
   * @return
   */
  public String getAuthors() {
    return this.authors;
  }

  /**
   * @param authors
   */
  public void setAuthors(String authors) {
    this.authors = authors;
  }

  /**
   * @return
   */
  public String getEditors() {
    return this.editors;
  }

  /**
   * @param editors
   */
  public void setEditors(String editors) {
    this.editors = editors;
  }

  /**
   * @return
   */
  public String getApiKey() {
    return this.apiKey;
  }

  /**
   * @param apiKey
   */
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

}
