package org.classicmayan.tools;

/**
 *
 */
public class Identifier {

  private String identifier;

  /**
   * @param identifier
   */
  public Identifier(String identifier) {
    this.setIdentifier(identifier);
  }

  /**
   * @param identifier
   */
  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  /**
   * @return
   */
  public String getIdentifier() {
    return this.identifier;
  }

}
