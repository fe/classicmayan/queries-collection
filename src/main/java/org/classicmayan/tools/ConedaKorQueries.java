/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 */
public class ConedaKorQueries {

  private static final String JSON_SUFFIX = ".json";
  private static final String DEFAULT_CHARSET = "UTF-8";
  protected static final String KOR_URL = "https://classicmayan.kor.de.dariah.eu/";

  /**
   * @param url
   * @return
   * @throws IOException
   * @throws JSONException
   */
  public static JSONObject readJsonObjectFromUrl(String url) throws IOException, JSONException {

    InputStream is = new URL(url).openStream();

    try {
      BufferedReader rd =
          new BufferedReader(new InputStreamReader(is, Charset.forName(DEFAULT_CHARSET)));
      String jsonText = readAll(rd);
      JSONObject json = new JSONObject(jsonText);

      return json;

    } finally {
      is.close();
    }
  }

  /**
   * @param rd
   * @return
   * @throws IOException
   */
  private static String readAll(Reader rd) throws IOException {

    StringBuilder sb = new StringBuilder();

    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }

    return sb.toString();
  }

  /**
   * @param mediaID
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public static String getEntityIDbyItsMediumID(String mediaID) throws JSONException, IOException {

    String entityIDforMedium = ConedaKorQueries
        .readJsonObjectFromUrl(KOR_URL + "resolve/medium-id/" + mediaID + JSON_SUFFIX).get("id")
        .toString();

    return entityIDforMedium;
  }

  /**
   * @param mediaID
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject getMediumMetadaSetForMetsMods(String mediaID)
      throws JSONException, IOException {

    JSONObject metadataSet = new JSONObject();
    try {
      metadataSet = new JSONObject(ConedaKorQueries.readJsonObjectFromUrl(KOR_URL + "entities/"
          + getEntityIDbyItsMediumID(mediaID) + JSON_SUFFIX + "?include=dataset,technical")
          .toString(2));
    } catch (org.json.JSONException allreadyMedia) {
      metadataSet = new JSONObject(
          ConedaKorQueries
              .readJsonObjectFromUrl(
                  KOR_URL + "entities/" + mediaID + JSON_SUFFIX + "?include=dataset,technical")
              .toString(2));
    }

    JSONObject specificMetadata = new JSONObject();
    specificMetadata.put("<dateCrated>",
        metadataSet.getJSONObject("dataset").get("date_time_created"));
    specificMetadata.put("<recordIdentifier(returnedEntity)>", metadataSet.get("id"));
    specificMetadata.put("<medium>", metadataSet.get("medium"));
    specificMetadata.put("<title>", metadataSet.getJSONObject("dataset").get("image_description"));
    specificMetadata.put("<copryrightHolder>",
        metadataSet.getJSONObject("dataset").get("rights_holder"));
    specificMetadata.put("<imageCreator>", metadataSet.getJSONObject("dataset").get("creator"));
    specificMetadata.put("<imageLicense>", metadataSet.getJSONObject("dataset").get("license"));

    try {
      specificMetadata.put("<license_note>",
          metadataSet.getJSONObject("dataset").get("license_note"));
    } catch (JSONException notFound) {
      // TODO Handle exception!
      System.out.println("ERROR: " + notFound);
    }

    return specificMetadata;
  }

  /**
   * @param mediaID
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject getMediumMetadaSetForMetsModsByDirectID(String mediaID)
      throws JSONException, IOException {

    JSONObject metadataSet = new JSONObject(ConedaKorQueries
        .readJsonObjectFromUrl(
            KOR_URL + "entities/" + mediaID + JSON_SUFFIX + "?include=dataset,technical")
        .toString(2));

    return metadataSet;
  }

  /**
   * @param kindID
   * @param perPage
   * @param pageNumber
   * @param from
   * @param to
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject getMediaList(String kindID, String perPage, String pageNumber,
      String from, String to) throws JSONException, IOException {

    String url = "";
    if (from == null && to == null) {
      System.out.println("NOTHING");
      url = KOR_URL + "entities.json?kind_id=" + kindID + "&per_page=" + perPage + "&page="
          + pageNumber + "&include=technical,dataset";
    }
    if (from != null && to == null) {
      url = KOR_URL + "entities.json?kind_id=" + kindID + "&created_after=" + from + "&per_page="
          + perPage + "&page=" + pageNumber + "&include=technical,dataset";
    }
    if (to != null && from == null) {
      System.out.println("ZAP");
      url = KOR_URL + "entities.json?kind_id=" + kindID + "&created_before=" + to + "&per_page="
          + perPage + "&page=" + pageNumber + "&include=technical,dataset";
    }
    if (to != null && from != null) {
      url = KOR_URL + "entities.json?kind_id=" + kindID + "&created_after=" + from
          + "&created_before=" + to + "&per_page=" + perPage + "&page=" + pageNumber
          + "&include=technical,dataset";
    }

    JSONObject json = readJsonObjectFromUrl(url);

    return json;
  }

  /**
   * <p>
   * Get all objects from ConedaKOR created before theDate, and modified after theDate. -->
   * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&created_before=2022-03-18&updated_after=2022-03-18&include=technical,dataset>
   * </p>
   * 
   * @param theDate
   * @param perPage
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject getImageMetadataUpdates(String theDate, int perPage)
      throws JSONException, IOException {

    JSONObject result = null;

    String url = KOR_URL + "entities.json?" +
        "kind_id=1" +
        "&per_page=" + String.valueOf(perPage) +
        "&created_before=" + theDate +
        "&updated_after=" + theDate +
        "&include=technical,dataset";

    result = readJsonObjectFromUrl(url);

    return result;
  }

  /**
   * <p>
   * Get all newly created objects from ConedaKOR after theDate. -->
   * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&created_after=2022-03-18&include=technical,dataset>
   * </p>
   * 
   * @param theDate
   * @param perPage
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject getLatestImageMetadataCreations(String theDate, int perPage)
      throws JSONException, IOException {

    JSONObject result = null;

    String url = KOR_URL + "entities.json?" +
        "kind_id=1" +
        "&per_page=" + String.valueOf(perPage) +
        "&created_after=" + theDate +
        "&include=technical,dataset";

    result = readJsonObjectFromUrl(url);

    return result;
  }

  /**
   * <p>
   * Get all objects from ConedaKOR after theDate. -->
   * <https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&include=technical,dataset>
   * </p>
   * 
   * @param theDate
   * @param perPage
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject getAllImageMetadata(int perPage, int page)
      throws JSONException, IOException {

    JSONObject result = null;

    String url = KOR_URL + "entities.json?" +
        "kind_id=1" +
        "&per_page=" + String.valueOf(perPage) +
        "&page=" + String.valueOf(page) +
        "&include=technical,dataset";

    result = readJsonObjectFromUrl(url);

    return result;
  }

}
