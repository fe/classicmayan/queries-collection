/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.util.HashMap;
import java.util.List;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.json.JSONObject;

/**
 *
 */
public class QueryUtillities {

  /**
   * @param fields
   * @param queries
   * @return
   */
  public String buildQuery(List<String> fields, HashMap<String, String> queries) {

    // TODO Level in own field!

    String query = "WHERE { GRAPH <URI_TO_REPLACE> {\n";
    String selects = "";
    for (String field : fields) {
      query = query.concat(queries.get(field));
      selects = selects.concat(
          "(group_concat( distinct ?" + field + " ;separator=\"; \") as ?" + field + "s)\n");
    }

    return IdiomConstants.SPARQL_PREFIXES + "\nSELECT " + selects + "\n\n" + query + " }}";
  }

  /**
   * @param query
   * @return
   */
  public static ResultSet executeQuery(String query) {

    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    return results;
  }

  /**
   * @param results
   * @param fieldToGetFromResponse
   * @return
   */
  public String queryExecution(ResultSet results, String fieldToGetFromResponse) {

    QuerySolution processQuery = results.nextSolution();

    return processQuery.get(fieldToGetFromResponse).toString();
  }

  /**
   * @param results
   * @param fields
   * @return
   */
  public JSONObject queryExecutionList(ResultSet results, List<String> fields) {

    JSONObject jsonObject = new JSONObject();
    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      for (String field : fields) {
        String fieldConent = processQuery.get(field + "s").toString();
        if (fieldConent.contains("; ")) {
          jsonObject.putOnce(field, SignQueries.getTransliteration(field, fieldConent));
        } else {
          jsonObject.accumulate(field, processQuery.get(field + "s"));
        }
      }
    }

    return jsonObject;
  }

  /**
   * @return
   * @deprecated
   */
  @Deprecated
  public JSONObject buildJSONObject() {
    JSONObject jsonObject = new JSONObject();

    return jsonObject;
  }

}
