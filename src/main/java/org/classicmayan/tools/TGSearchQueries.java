/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.NotFoundException;
import info.textgrid.clients.AuthClient;
import info.textgrid.clients.SearchClient;
import info.textgrid.clients.tgauth.AuthClientException;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

/**
 *
 */
public class TGSearchQueries {

  // **
  // STATIC
  // **

  private static SearchClient searchClient;

  // **
  // CLASS
  // **

  private String textgridSearchEndpoint;
  private String projectID;
  private String sessionID;
  private String formatToFilter = "format:text/xml";
  private int querySize = 1000;

  /**
   * @throws IOException
   */
  public TGSearchQueries(final String propertiesFile) throws IOException {

    String props;
    if (propertiesFile != null && !propertiesFile.isEmpty()) {
      props = propertiesFile;
    } else {
      props = IdiomConstants.PROPERTIES_FILE;
    }

    try (InputStream input = new FileInputStream(props)) {
      Properties p = new Properties();
      p.load(input);

      this.sessionID = p.getProperty(IdiomConstants.SID_PROPERTY);
      this.projectID = p.getProperty(IdiomConstants.PID_PROPERTY);
      this.formatToFilter = p.getProperty(IdiomConstants.FORMAT_TO_FILTER_PROPERTY);
      this.querySize = Integer.parseInt(p.getProperty(IdiomConstants.QUERY_SIZE_PROPERTY));
      this.textgridSearchEndpoint = p.getProperty(IdiomConstants.TGSEARCH_ENDPOINT_PROPERTY);
      if (this.textgridSearchEndpoint == null || this.textgridSearchEndpoint.equals("")) {
        this.textgridSearchEndpoint = SearchClient.DEFAULT_NONPUBLIC_ENDPOINT;
      }

      input.close();

      // Then create singleton TG-crud client.
      if (searchClient == null) {
        searchClient = new SearchClient(this.textgridSearchEndpoint).enableGzipCompression();
      }
    }
  }

  /**
   * @param sid
   * @return
   * @throws AuthClientException
   */
  public String getNameBySid(String sid) throws AuthClientException {
    AuthClient auth = new AuthClient();
    return auth.getEppnForSid(sid);
  }

  /**
   * <p>
   * This function return a hash map containing all TEI files in the textgrid repository of the
   * IDIOM project. Because the access is limited to members of the projects a session id is
   * required.
   * </p>
   * 
   * @return
   * @throws IOException
   */
  public HashMap<String, String> getTitleWithURI() throws IOException {

    HashMap<String, String> titleWithURIsForTextcarrier = new HashMap<String, String>();

    Response response = searchClient.searchQuery()
        .setSid(this.getSessionID())
        .setQuery("project.id:" + this.getProjectID())
        .addFilter(this.getFormatToFilter())
        .setOrder("asc:title")
        .setLimit(this.getQuerySize())
        .execute();

    // Put textgridUris and title into the HashMap.
    for (ResultType result : response.getResult()) {
      String tguri = result.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      String title = result.getObject().getGeneric().getProvided().getTitle().get(0);

      if (result.getObject().getGeneric().getProvided().getNotes() != null
          && result.getObject().getGeneric().getProvided().getNotes().equals("PARSER_FILE")) {
        titleWithURIsForTextcarrier.put(tguri, title);
      }
    }

    return titleWithURIsForTextcarrier;
  }

  /**
   * @return
   * @throws IOException
   */
  public HashMap<String, String> getTextCarrierFiles() throws IOException {

    HashMap<String, String> titleWithURIsForTextcarrier = new HashMap<String, String>();

    Response response = searchClient.searchQuery()
        .setSid(this.getSessionID())
        .setQuery("project.id:" + this.getProjectID())
        .addFilter(this.getFormatToFilter())
        .setOrder("asc:title")
        .setLimit(this.getQuerySize())
        .execute();

    // Put textgridUris and title into the HashMap.
    for (ResultType result : response.getResult()) {
      if (result.getObject().getGeneric().getProvided().getNotes() != null
          && result.getObject().getGeneric().getProvided().getNotes().equals("PARSER_FILE")) {
        String tguri = result.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
        String title = result.getObject().getGeneric().getProvided().getTitle().get(0);
        titleWithURIsForTextcarrier.put(tguri, title);
      }
    }

    return titleWithURIsForTextcarrier;
  }

  /**
   * <p>
   * Get URI of the latest subcollection of a collection.
   * </p>
   * 
   * @param rootCollectionURI
   * @return
   */
  public ObjectType getLatestSubcollectionObject(String rootCollectionURI) {

    ObjectType result = null;

    // Get all aggregations of collection, uses
    // <https://textgridlab.org/1.0/tgsearch/navigation/agg/textgrid:3x63n.0>.
    List<ResultType> resultList = searchClient.navigationQuery()
        .setSid(this.sessionID)
        .listAggregation(rootCollectionURI)
        .getResult();

    // Get latest object as result.
    result = resultList.get(resultList.size() - 1).getObject();

    return result;
  }

  /**
   * @return
   */
  public int getChildren(String collectionURI) {

    int result = 0;

    // Get all entries of collection, uses
    // <https://dev.textgridlab.org/1.0/tgsearch/info/textgrid:42j3z.0/children>
    // NOTE We have to use children here, not navigation, we need every object from the aggregation,
    // not only the displayed ones (such as deleted or the forbidden ones)!
    List<String> uriList = searchClient.infoQuery()
        .setSid(this.sessionID)
        .getChildren(collectionURI)
        .getTextgridUri();

    // Get amount.
    result = uriList.size();

    return result;
  }

  /**
   * @return
   */
  public String getLatestObjectURI(String collectionURI) {

    String result = null;

    // Get all entries of collection, uses
    // <https://dev.textgridlab.org/1.0/tgsearch/info/textgrid:42j3z.0/children>
    List<String> uriList = searchClient.infoQuery()
        .setSid(this.sessionID)
        .getChildren(collectionURI)
        .getTextgridUri();

    // Get latest object as result.
    if (!uriList.isEmpty()) {
      result = uriList.get(uriList.size() - 1);
    }

    return result;
  }

  /**
   * <p>
   * Get metadata for an object.
   * </p>
   * 
   * @param uri
   * @return
   */
  public ObjectType getMetadata(String uri) {

    ObjectType result = null;

    // Get metadata of an entry, uses
    // <https://dev.textgridlab.org/1.0/tgsearch/info/textgrid:42j3z.0/metadata>
    result = searchClient.infoQuery()
        .setSid(this.sessionID)
        .getMetadata(uri)
        .getResult().get(0)
        .getObject();

    return result;
  }

  /**
   * <p>
   * <notes>(notes:"ConedaKorMediumData 26349")</notes> OR (if not existing) we are using the title
   * tag.
   * </p>
   * 
   * @param korID
   * @return
   * @throws NotFoundException
   */
  public String getTextGridURIfromKorID(String korID) throws NotFoundException {

    String result = "";

    // Check title.
    String titleURI = "";
    String titleQuery = "title:\"[" + korID + "]\"";
    List<ResultType> titleList = searchClient.searchQuery()
        .setSid(this.getSessionID())
        .setQuery("project.id:" + this.getProjectID())
        .setQuery(titleQuery)
        .execute()
        .getResult();
    if (titleList.size() != 0) {
      titleURI =
          titleList.get(0).getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    }

    // Check notes.
    String notesURI = "";
    String notesQuery = "(notes:\"ConedaKorMediumData " + korID + "\")";
    List<ResultType> notesList = searchClient.searchQuery()
        .setSid(this.getSessionID())
        .setQuery("project.id:" + this.getProjectID())
        .setQuery(notesQuery)
        .execute()
        .getResult();
    if (notesList.size() != 0) {
      notesURI =
          titleList.get(0).getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    }

    // Check for empty node lists.
    if (notesURI.isEmpty() && titleURI.isEmpty()) {
      throw new NotFoundException("KOR ID not found in notes and title!");
    }

    // Check for equality.
    if (notesURI.equals(titleURI)) {
      result = notesURI;
    } else if (notesURI.isEmpty()) {
      System.out.print("[WARNING! No KOR ID in <notes> for " + korID + "] ");
      result = titleURI;
    } else {
      System.out.print("[WARNING! KOR ID in <notes> differs from KOR ID in title: " + korID + "] ");
      result = notesURI;
    }

    return result;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public String getProjectID() {
    return this.projectID;
  }

  /**
   * @param projectID
   */
  public void setProjectID(String projectID) {
    this.projectID = projectID;
  }

  /**
   * @return
   */
  public String getSessionID() {
    return this.sessionID;
  }

  /**
   * @param sessionID
   */
  public void setSessionID(String sessionID) {
    this.sessionID = sessionID;
  }

  /**
   * @return
   */
  public String getFormatToFilter() {
    return this.formatToFilter;
  }

  /**
   * @param formatToFilter
   */
  public void setFormatToFilter(String formatToFilter) {
    this.formatToFilter = formatToFilter;
  }

  /**
   * @return
   */
  public int getQuerySize() {
    return this.querySize;
  }

  /**
   * @param querySize
   */
  public void setQuerySize(int querySize) {
    this.querySize = querySize;
  }

  /**
   * @return
   */
  public String getTextgridSearchEndpoint() {
    return this.textgridSearchEndpoint;
  }

  /**
   * @param textgridSearchEndpoint
   */
  public void setTextgridSearchEndpoint(String textgridSearchEndpoint) {
    this.textgridSearchEndpoint = textgridSearchEndpoint;
  }

}
