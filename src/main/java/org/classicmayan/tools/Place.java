package org.classicmayan.tools;

/**
 *
 */
public class Place {

  private Identifier identifier;
  private String placeName;
  private String latitude;
  private String longitude;
  private String altitude;
  private String archeologicalCoordinates;
  private String locatedPlaceNames;

  /**
   * @return
   */
  public Identifier getIdentifier() {
    return this.identifier;
  }

  /**
   * @return
   */
  public String getLocatedPlaceNames() {
    return this.locatedPlaceNames;
  }

  /**
   * @param locatedPlaceNames
   */
  public void setLocatedPlaceNames(String locatedPlaceNames) {
    this.locatedPlaceNames = locatedPlaceNames;
  }

  /**
   * @return
   */
  public String getArcheologicalCoordinates() {
    return this.archeologicalCoordinates;
  }

  /**
   * @param archeologicalCoordinates
   */
  public void setArcheologicalCoordinates(String archeologicalCoordinates) {
    this.archeologicalCoordinates = archeologicalCoordinates;
  }

  /**
   * @return
   */
  public String getAltitude() {
    return this.altitude;
  }

  /**
   * @param altitude
   */
  public void setAltitude(String altitude) {
    this.altitude = altitude;
  }

  /**
   * @return
   */
  public String getLongitude() {
    return this.longitude;
  }

  /**
   * @param longitude
   */
  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  /**
   * @return
   */
  public String getLatitude() {
    return this.latitude;
  }

  /**
   * @param latitude
   */
  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  /**
   * @return
   */
  public String getPlaceName() {
    return this.placeName;
  }

  /**
   * @param placeName
   */
  public void setPlaceName(String placeName) {
    this.placeName = placeName;
  }

  /**
   * @param identifier
   */
  public void setIdentifier(Identifier identifier) {
    this.identifier = identifier;
  }

}
