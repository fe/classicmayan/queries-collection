/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.util.HashMap;
import java.util.List;

import org.apache.jena.query.ResultSet;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 */
public class SignQueries extends QueryUtillities {

  private String textgridURI;
  private ResultSet signResult;

  /**
   * 
   */
  private static final HashMap<String, String> signQueries = new HashMap<String, String>() {
    private static final long serialVersionUID = 1L;
    {
      put("status", "?textgridURI idiom:status ?status. \n");
      put("signNumber", "?textgridURI idiomcat:signNumber ?signNumber.\n");
      put("logographic_reading", IdiomConstants.SIGN_LOGOGRAPHIC_READING);
    }
  };

  /**
   * @param textgridURI
   * @param fields
   */
  public SignQueries(String textgridURI, List<String> fields) {
    this.textgridURI = textgridURI;
  }

  /**
   * @param transliterationType
   * @param transliterationContent
   * @return
   */
  public static JSONArray getTransliteration(String transliterationType,
      String transliterationContent) {

    JSONArray transliterations = new JSONArray();
    String[] fieldConents = transliterationContent.split("; ");
    int i = 0;
    for (String fieldContent : fieldConents) {
      JSONObject subFields = new JSONObject();
      subFields.put("value", fieldContent.split(":")[0]);
      subFields.put("level", fieldContent.split(":")[1]);
      transliterations.put(i, subFields);
      i++;
    }

    return transliterations;
  }

  /*
   * public void executeSignQuery(String query) { signResult =
   * QueryUtillities.executeQuery(query.replace("URI_TO_REPLACE", textgridURI)); }
   */

  /**
   * @return
   */
  public String getTextgridURI() {
    return this.textgridURI;
  }

  /**
   * @param textgridURI
   */
  public void setTextgridURI(String textgridURI) {
    this.textgridURI = textgridURI;
  }

  /**
   * @return
   */
  public HashMap<String, String> getSignQueries() {
    return signQueries;
  }

  /**
   * @return
   */
  public ResultSet getSignResult() {
    return this.signResult;
  }

  /**
   * @param signResult
   */
  public void setSignResult(ResultSet signResult) {
    this.signResult = signResult;
  }

}
