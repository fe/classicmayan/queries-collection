package org.classicmayan.tools;

/**
 *
 */
public class Dedication {

  private Identifier identifier;
  private String title;
  private String placeName;
  private Date datesInDifferentFormats;

  /**
   * @return
   */
  public Identifier getIdentifier() {
    return this.identifier;
  }

  /**
   * @return
   */
  public Date getDatesInDifferentFormats() {
    return this.datesInDifferentFormats;
  }

  /**
   * @param datesInDifferentFormats
   */
  public void setDatesInDifferentFormats(Date datesInDifferentFormats) {
    this.datesInDifferentFormats = datesInDifferentFormats;
  }

  /**
   * @return
   */
  public String getPlaceName() {
    return this.placeName;
  }

  /**
   * @param placeName
   */
  public void setPlaceName(String placeName) {
    this.placeName = placeName;
  }

  /**
   * @return
   */
  public String getTitle() {
    return this.title;
  }

  /**
   * @param title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @param identifier
   */
  public void setIdentifier(Identifier identifier) {
    this.identifier = identifier;
  }

}
