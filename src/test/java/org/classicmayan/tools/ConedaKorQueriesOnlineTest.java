/**
 * This software is copyright (c) 2023 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;

/**
 *
 */
@Ignore
public class ConedaKorQueriesOnlineTest {

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  public void testGetMediaList() throws JSONException, IOException {
    System.out.println(
        ConedaKorQueries.getMediaList("1", "100", "1", "2016-06-01", "2019-06-01").toString(2));
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  public void testGenericKorQuery() throws JSONException, IOException {
    System.out.println(ConedaKorQueries.readJsonObjectFromUrl(
        "https://classicmayan.kor.de.dariah.eu/entities.json?kind_id=1&per_page=100&page=100&include=technical,dataset")
        .toString(2));
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  public void getMetadataSetForMediaWithEntityID() throws JSONException, IOException {
    for (String mediaID : TripleStoreQuery.getLinkedMediaInKorFromArtefact("textgrid:3bxs0")) {
      System.out.println(ConedaKorQueries.getMediumMetadaSetForMetsMods(mediaID).toString(2));
    }
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  public void getEntityIDForMediumByItsMediumID() throws JSONException, IOException {
    for (String mediaID : TripleStoreQuery.getLinkedMediaInKorFromArtefact("textgrid:3bxs0")) {
      System.out.println(ConedaKorQueries.getEntityIDbyItsMediumID(mediaID));
    }
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  public void getDirectly() throws JSONException, IOException {
    System.out
        .println(ConedaKorQueries.getMediumMetadaSetForMetsModsByDirectID("13470").toString(2));
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws org.codehaus.jettison.json.JSONException
   * @throws CrudClientException
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   */
  @Test
  public void LoopThroughtCompleteMediaList() throws JSONException, IOException,
      org.codehaus.jettison.json.JSONException, CrudClientException, ObjectNotFoundFault, AuthFault,
      IoFault, MetadataParseFault, ProtocolNotImplementedFault {

    // System.out.println(ConedaKorQueries.getMediaList("1", "100", "1", "", "").toString(2));
    JSONArray mediaList =
        ConedaKorQueries.getMediaList("1", "100", "49", "", "").getJSONArray("records");
    for (int pageNumber = 49; mediaList.length() * pageNumber < 4901; pageNumber++) {
      mediaList = ConedaKorQueries.getMediaList("1", "100", Integer.toString(pageNumber), "", "")
          .getJSONArray("records");
      System.out.println(mediaList.length() * pageNumber);
      for (int i = 0; i < mediaList.length(); i++) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
        // JSONObject json = new JSONObject(mediaList.get(i).toString());
        // ConeKorImagesToTextGrid.transferConedaKorMediumMetadataToTextGrid(mediaList.get(i).toString());
      }
    }
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws CrudClientException
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   */
  @Test
  public void storeSingleFileFromConedaKorToTG()
      throws JSONException, IOException, CrudClientException, ObjectNotFoundFault, AuthFault,
      IoFault, MetadataParseFault, ProtocolNotImplementedFault {

    JSONObject json = ConedaKorQueries.getMediumMetadaSetForMetsModsByDirectID("13470");
    System.out.println(json);
    // ConeKorImagesToTextGrid.transferConedaKorMediumMetadataToTextGrid(json.toString(2));
  }

}
