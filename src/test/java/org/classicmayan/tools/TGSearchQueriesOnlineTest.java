/**
 * This software is copyright (c) 2023 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.IOException;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.jdom2.JDOMException;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;
import info.textgrid.clients.tgauth.AuthClientException;
import info.textgrid.clients.tgcrud.CrudClientException;

/**
 * FIXME: Check tests with valid TextGrid Session ID!
 */
@Ignore
public class TGSearchQueriesOnlineTest {

  static IdiomTextGridObject idiomTGObject;
  static TGSearchQueries tgSearchQueries;

  /**
   * @throws CrudClientException
   * @throws IOException
   *
   */
  @BeforeClass
  public static void setUp() throws IOException, CrudClientException {

    idiomTGObject = new IdiomTextGridObject(null);
    tgSearchQueries = new TGSearchQueries(null);

    tgSearchQueries.setSessionID(idiomTGObject.getSessionID());
    tgSearchQueries.setProjectID(idiomTGObject.getProjectID());
    tgSearchQueries.setFormatToFilter(idiomTGObject.getFormatToFilter());
    tgSearchQueries.setQuerySize(Integer.parseInt(idiomTGObject.getQuerySize()));
  }

  /**
   * @throws AuthClientException
   */
  @Test
  public void testGetEPPNforSid() throws AuthClientException {
    System.out.println(tgSearchQueries.getNameBySid(""));
  }

  /**
   * @throws CrudClientException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testUpdateObjects() throws CrudClientException, IOException {
    IdiomTextGridObject ob = new IdiomTextGridObject(null);
    ob.updateObject();
  }

  /**
   * @throws IOException
   */
  @Test
  @Ignore
  public void testGetTGData() throws IOException {
    System.out.println(idiomTGObject.getTextGridObjectByUri("textgrid:3rb8d.0", "", 80));
  }

  /**
   * @throws CrudClientException
   */
  @Test
  @Ignore
  public void testConsistencyArtefact() throws CrudClientException {
    idiomTGObject.consistencyCheckArtefact();
  }

  /**
   * @throws IOException
   */
  @Test
  @Ignore
  public void testTGSearchQuery() throws IOException {

    TGSearchQueries searchQueries = new TGSearchQueries(null);

    System.out.println("-------------- TGSearch for Title and URI ---------------\n");

    HashMap<String, String> titleWithURIsForTextcarrier = new HashMap<String, String>();
    titleWithURIsForTextcarrier = searchQueries.getTextCarrierFiles();
    System.out.println(titleWithURIsForTextcarrier.size());
    for (String name : titleWithURIsForTextcarrier.keySet()) {
      String key = name.toString();
      String value = titleWithURIsForTextcarrier.get(name).toString();
      System.out.println(key + " | " + value);
    }
  }

  /**
   * @throws CrudClientException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testGetTextGridDataObjectAsString() throws CrudClientException, IOException {
    System.out.println(
        "-------------- Get the TextGrid Data Object as String by a given URI ---------------\n");
    // idiomTGObject.setTextGridURI("textgrid:3r9v0.0");
    System.out.println(idiomTGObject.getDataAsString("textgrid:3r9v0.0"));
  }

  /**
   * @throws IOException
   * @throws CrudClientException
   * @throws SAXException
   * @throws JDOMException
   * @throws ParserConfigurationException
   */
  @Test
  @Ignore
  public void testGetTextGridDataObjectAsXML() throws IOException, CrudClientException,
      SAXException, JDOMException, ParserConfigurationException {
    System.out.println(
        "-------------- Get the TextGrid Data Object as XML by a given URI ---------------\n");

    // idiomTGObject.setTextGridURI("textgrid:37qgs.0");
    // DOMSource source = new DOMSource(idiomTGObject.getDataAsXMLObject("textgrid:37qgs.0"));
    // JDOMSource source = new JDOMSource(idiomTGObject.getDataAsXMLObject("textgrid:37qgs.0"));
    // StreamResult xmlOutput = new StreamResult(new StringWriter());

    XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    xout.output(idiomTGObject.getDataAsXMLObject("textgrid:37qgs.0"), System.out);

    /*
     * Transformer transformer = TransformerFactory.newInstance().newTransformer();
     * transformer.setOutputProperty(OutputKeys.INDENT, "yes");
     * transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
     * transformer.transform(source, xmlOutput);
     */

    // System.out.println(xmlOutput.getWriter());
  }

  /**
   * @throws CrudClientException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testSaveToTextGrid() throws IOException, CrudClientException {

    // IDIOMTextGridObject uri = new IDIOMTextGridObject();

    String jsonFile = "{\"menu\": {\n" +
        "  \"id\": \"file\",\n" +
        "  \"value\": \"File\",\n" +
        "  \"popup\": {\n" +
        "    \"menuitem\": [\n" +
        "      {\"value\": \"New\", \"onclick\": \"CreateNewDoc()\"},\n" +
        "      {\"value\": \"Open\", \"onclick\": \"OpenDoc()\"},\n" +
        "      {\"value\": \"Close\", \"onclick\": \"CloseDoc()\"}\n" +
        "    ]\n" +
        "  }\n" +
        "}}";
    JSONObject json = new JSONObject(jsonFile);

    idiomTGObject.saveToTextGrid(json, "Uxul Stela_666", false);
  }

}
