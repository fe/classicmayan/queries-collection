/**
 * This software is copyright (c) 2023 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.IOException;
import org.json.JSONException;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;

/**
 *
 */
@Ignore
public class ConedaKor2TextGridRepTest {

  /**
   * @throws CrudClientException
   * @throws JSONException
   * @throws IOException
   * @throws ProtocolNotImplementedFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   */
  @Test
  public void testUpdateAndImportNew() throws IOException, CrudClientException, JSONException,
      ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault, ProtocolNotImplementedFault {

    ConedaKor2TextGridRep main = new ConedaKor2TextGridRep();
    main.setPropertiesFileLocation(null);

    main.preparations();
    main.updateExisting();
    main.importNew();
  }

}
