/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;

/**
 * <p>
 * Unit test for SPARQL queries.
 * </p>
 */
@Ignore
public class SOARQLQueryOnlineTest {

  Artefact artefact;

  /**
   * @throws IOException
   */
  @Test
  @Ignore
  public void testSwedishWorkshopQuery() throws IOException {

    String csv = "";
    for (Artefact a : TripleStoreQuery.swedishWorkShopQuery()) {

      System.out.println("...processing artefact: " + a.getIdentifier());

      csv = csv.concat(a.getIdentifier().getIdentifier() + ";");
      csv = csv.concat(a.getIdiomNumber() + ";");
      csv = csv.concat(a.getPreferredTitle() + ";");
      try {
        csv = csv.concat(a.getMaterial() + ";");
      } catch (NullPointerException e) {
        csv = csv.concat("UNKNOWN;");
      }

      try {
        csv = csv.concat(a.getArtefactType() + ";");
      } catch (NullPointerException e) {
        csv = csv.concat("UNKNOWN;");
      }

      csv = csv.concat(a.getIdentifier().getIdentifier() + ";");
      // System.out.println(
      // artefact.getIdentifier().getIdentifier() + ";" +
      // artefact.getIdiomNumber() + ";" +
      // artefact.getPreferredTitle() + ";" +
      // artefact.getMaterial() + ";" +
      // artefact.getArtefactType() + ";" +
      // artefact.getPlace().getPlaceName() + ";" +
      // artefact.getPlace().getLatitude() + ";" +
      // artefact.getPlace().getLongitude()
      // );
      try {
        csv = csv.concat(a.getPlace().getPlaceName() + ";");
      } catch (NullPointerException e) {
        csv = csv.concat("UNKNOWN;");
      }
      try {
        csv = csv.concat(a.getPlace().getLatitude() + ";");
      } catch (NullPointerException e) {
        csv = csv.concat("UNKNOWN;");
      }
      try {
        csv = csv.concat(a.getPlace().getLongitude() + ";");
      } catch (NullPointerException e) {
        csv = csv.concat("UNKNOWN;");
      }
      try {
        csv = csv.concat(a.getDedicationDate() + ";");
      } catch (NullPointerException e) {
        csv = csv.concat("UNKNOWN;");
      }
      try {
        csv = csv
            .concat(a.getDedicationGregDate().substring(0, a.getDedicationGregDate().indexOf("-")));
      } catch (NullPointerException e) {
        csv = csv.concat("UNKNOWN;");
      }

      csv = csv.concat("\n");
    }

    whenWriteStringUsingBufferedWritter_thenCorrect(csv);
  }

  /**
   * @param csv
   * @throws IOException
   */
  public void whenWriteStringUsingBufferedWritter_thenCorrect(String csv) throws IOException {

    try {
      File myObj = File.createTempFile("queries-collection-", ".csv");
      if (myObj.createNewFile()) {
        System.out.println("File created: " + myObj.getCanonicalPath());

        FileWriter myWriter = new FileWriter(myObj);
        myWriter.write(csv);
        myWriter.close();

        System.out.println("Successfully wrote to the file.");
      } else {
        System.out.println("File already exists.");
      }
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
  }

  /**
   * 
   */
  @Test
  public void testArtefactsWithMediaLinks() {
    System.out.println(TripleStoreQuery.getArtefactsWithLinksToKor());
  }

  /**
   * 
   */
  @Test
  public void testGetLinkedMedia() {
    System.out.println(TripleStoreQuery.getLinkedMediaInKorFromArtefact("textgrid:3bxs0"));
  }

  /**
   * 
   */
  public void testGetPlaceNamRecusrive() {
    System.out.println(TripleStoreQuery.getPlaceRescursive("textgrid:350mb"));

  }

  /**
   * 
   */
  @Test
  public void testGetArtefactImages() {
    System.out.println(TripleStoreQuery.getArtefactImages("textgrid:3w27t"));
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testGetAllDiscoveriesLinkedWithPlace() {

    System.out.println(TripleStoreQuery.getAllDiscoveriesLinkedToPlace("Guatemala").size());

    /*
     * for(String place : TripleStoreQuery.getAllDiscoveriesLinkedWithPlace()) {
     * System.out.println(place); }
     */
  }

  /**
   * 
   */
  @Test
  public void testGetImageOfGraph() {
    System.out.println(TripleStoreQuery.getImageOfGraph("textgrid:2skxn"));
  }

  /**
   * 
   */
  @Test
  public void testSearchActivitiy() {

    List<Activity> activities = TripleStoreQuery.searchInActivities("quatsch");
    System.out.println("HITS: " + activities.size() + "\n");
    for (Activity activity : activities) {
      System.out.println(activity.getTitle());
      System.out.println(activity.getDatesInDifferentFormats().getCalenderRoundTimeSpan());
      System.out.println(activity.getDatesInDifferentFormats().getGregorianTimeSpan());
      System.out.println(activity.getDatesInDifferentFormats().getLongcountTimeSpan());
      System.out.println();
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchEpigraphicActor() {

    List<EpigraphicActor> epigraphicActors = TripleStoreQuery.searchInEpigraphicActors("Pacal");
    System.out.println("HITS: " + epigraphicActors.size() + "\n");
    for (EpigraphicActor actor : epigraphicActors) {
      System.out.println("prefName: " + actor.getActorName());
      System.out.println("altName: " + actor.getAltName());
      System.out.println("");
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchEpigraphicGroup() {

    List<EpigraphicGroup> epigraphicGroups = TripleStoreQuery.searchInEpigraphicGroups("");
    System.out.println("HITS: " + epigraphicGroups.size() + "\n");
    for (EpigraphicGroup group : epigraphicGroups) {
      System.out.println(group.getGroupName());
      System.out.println();
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchPlaces() {
    System.out.println();
    for (Place place : TripleStoreQuery.searchInPlaces("Menche")) {
      System.out.println("NAME: " + place.getPlaceName());
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchDedication() {
    System.out.println(TripleStoreQuery.searchInDedication(""));
  }

  /*
   * @Test public void testSignQueryHashMap() { QueryUtillities queryUtils = new QueryUtillities();
   * List<String> fields = new ArrayList<String>();
   * 
   * fields.add("status"); fields.add("signNumber"); fields.add("logographic_reading");
   * 
   * SignQueries sign = new SignQueries("textgrid:3r85m", fields); String query =
   * sign.buildQuery(fields, sign.getSignQueries()); ResultSet set =
   * sign.executeQuery(query.replace("URI_TO_REPLACE", sign.getTextgridURI()));
   * 
   * System.out.println(queryUtils.queryExecutionList(set, fields).toString(2)); }
   */

  /**
   * 
   */
  @Test
  public void testGetURIsofAllArtefacts() {

    List<String> artefactURIs = new ArrayList<String>();
    artefactURIs = TripleStoreQuery.getArtefactList();

    System.out.println(
        "--------------  Test to get the TextGrudURIs of all artefacts in IDIOM triplestore ---------------\n");
    System.out.println("Found " + artefactURIs.size() + " objects in triplestore\n");

    for (String uri : artefactURIs) {
      System.out.println(uri);
    }

    System.out.println("\n----------- End of AllArtefactTest ------------------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetSignNumberForURI() {

    System.out.println(
        "-------------- Test to get the SignNumber from a TextGrid URI in IDIOM triplestore ---------------\n");
    System.out.println("The textgridURI: textgrid:2skzc has the sign number: "
        + TripleStoreQuery.getSignNumberForUri("textgrid:2skzc") + "\n");
    System.out.println("--------------- End of getSignNumber test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testIsGraphOfSign() {
    System.out.println(TripleStoreQuery.getIsGraphOfSign("textgrid:2skhk"));
  }

  /**
   * 
   */
  @Test
  public void testGetLogographicReadingTransliterationValueWithConfidenceLevelWithGraphAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getLogographicReadingTransliterationValueWithLevel("textgrid:2skk0"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetLogographicReadingTransliterationValueWithConfidenceLevelWithSignAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getLogographicReadingTransliterationValueWithLevel("textgrid:2skjx"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetLogographicMeaningTransliterationValueWithConfidenceLevelWithGraphAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getLogographicMeaningTransliterationValueWithLevel("textgrid:3r4wn"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetLogographicMeaningTransliterationValueWithConfidenceLevelWithSignAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getLogographicMeaningTransliterationValueWithLevel("textgrid:36jdh"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetSyllabicReadingTransliterationValueWithConfidenceLevelWithGraphAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getSyllabicReadingTransliterationValueWithLevel("textgrid:2skhk"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetSyllabicReadingTransliterationValueWithConfidenceLevelWithSignAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getSyllabicReadingTransliterationValueWithLevel("textgrid:2skhj"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetDiacriticFunctionTransliterationValueWithConfidenceLevelWithGraphAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getDiacriticFunctionTransliterationValueWithLevel("textgrid:3rcg1"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetDiacriticFunctionTransliterationValueWithConfidenceLevelWithSignAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getDiacriticFunctionTransliterationValueWithLevel("textgrid:3rcg0"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetNumericFunctionTransliterationValueWithConfidenceLevelWithGraphAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getNumericFunctionTransliterationValueWithLevel("textgrid:30gnx"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetNumericFunctionTransliterationValueWithConfidenceLevelWithSignAsInput() {

    System.out.println(
        "-------------- Test to get the transliteration value of a logographic reading for a sign in IDIOM triplestore ---------------\n");
    System.out.println("The logographic reading and confidence level is: " +
        TripleStoreQuery.getNumericFunctionTransliterationValueWithLevel("textgrid:30gnx"));
    System.out.println("--------------- End of logographic reading test -----------\n");
  }

  /*
   * @Test public void testsearchArtefactByName() {
   * 
   * System.out.println("-------------- Test search artefact in IDIOM triplestore ---------------\n"
   * ); for(Artefact artefact : TripleStoreQuery.searchArtefactByName("e")) {
   * System.out.println("------ Artefact -------"); System.out.println("URI: " + artefact.getUri());
   * System.out.println("TITLE: " + artefact.getTitle()); //System.out.println("ADD_NAME: " +
   * artefact.getAdditionalIdentifierName()); //System.out.println("ADD_TYPE: " +
   * artefact.getAdditionalIdentifierType()); System.out.println("IDIOM_NUMBER: " +
   * artefact.getIdiomNumber()); System.out.println("------------------------\n"); }
   * 
   * System.out.println("---------------  End of artefact search test -----------\n"); }
   */

  /**
   * 
   */
  @Test
  public void testSearchActivitiyAsJavaObject() {

    for (Activity activity : TripleStoreQuery.searchInActivities("Tikal")) {
      System.out.println("TITLE: " + activity.getTitle());
      System.out.println("PLACE: " + activity.getPlaceName());
      for (String longcount : activity.getDatesInDifferentFormats().getLongcountTimeSpan()) {
        System.out.println("LC-TIMESPAN: " + longcount);
      }
      for (String gregDate : activity.getDatesInDifferentFormats().getGregorianTimeSpan()) {
        System.out.println("GREG-TIMESPAN: " + gregDate);
      }
      for (String calenderRound : activity.getDatesInDifferentFormats()
          .getCalenderRoundTimeSpan()) {
        System.out.println("CR: " + calenderRound);
      }
      System.out.println("TEXTGRID-URI: " + activity.getIdentifier().getIdentifier());

      System.out.println("\n\n\n");
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchEpigraphicGroupsAsJavaObject() {

    for (EpigraphicGroup epigraphicGroup : TripleStoreQuery.searchInEpigraphicGroups("")) {
      System.out.println("URI: " + epigraphicGroup.getIdentifier().getIdentifier());
      System.out.println("NAME: " + epigraphicGroup.getGroupName());
      System.out.println("RESIDENCE: " + epigraphicGroup.getResidenceTitle());
      System.out.println("GND: " + epigraphicGroup.getGndRef());
      System.out.println("\n\n\n");
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchInPlacesAsJavaObject() {

    for (Place place : TripleStoreQuery.searchInPlaces("Tikal")) {
      System.out.println("ALTITUDE: " + place.getAltitude());
      System.out.println("COORDINATES: " + place.getArcheologicalCoordinates());
      System.out.println("LATITUDE: " + place.getLatitude());
      System.out.println("LOCATED PLACENAME: " + place.getLocatedPlaceNames());
      System.out.println("LONGITUDE: " + place.getLongitude());
      System.out.println("PLACE NAME: " + place.getPlaceName());
      System.out.println("TEXTGRID: : " + place.getIdentifier().getIdentifier());
      System.out.println("\n\n\n");
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchInDedicationsAsJavaObject() {

    for (Dedication dedication : TripleStoreQuery.searchInDedication("")) {
      System.out.println("PLACE NAME: " + dedication.getPlaceName());
      System.out.println("TITLE: " + dedication.getTitle());
      System.out.println("TEXTGRID: " + dedication.getIdentifier().getIdentifier());
      for (String longcount : dedication.getDatesInDifferentFormats().getLongcountTimeSpan()) {
        System.out.println("LC-TIMESPAN: " + longcount);
      }
      System.out.println("\n\n\n");
    }
  }

  /**
   * 
   */
  @Test
  public void testSearchInEpigraphicActosAsJavaObject() {
    for (EpigraphicActor epigraphicActor : TripleStoreQuery.searchInEpigraphicActors("")) {
      System.out.println("ACTOR NAME: " + epigraphicActor.getActorName());
      System.out.println("DETAILS: " + epigraphicActor.getDetails());
      System.out.println("TEXTGRID: " + epigraphicActor.getIdentifier().getIdentifier());
      System.out.println("LIFEDATA: " + epigraphicActor.getLifeData());
      System.out.println("PMD: " + epigraphicActor.getPndID());
      System.out.println("\n\n\n");
    }
  }

  /**
   * 
   */
  @Test
  public void testGetTitleOfArtefactByTextgridUri() {

    System.out.println(
        "--------------- Test to get the Title of an artefact identified by a TextGridUri ---------------\n");
    System.out.println(TripleStoreQuery.getArtefactNameByTextGridUri("textgrid:3r111"));
    System.out.println("\n-------------- End of ArtefactNameTest ---------------\n");
  }

  /**
   * 
   */
  @Test
  public void testArtefactData() {

    this.artefact = new Artefact("textgrid:2sg18");

    System.out.println(this.artefact.getPreferredTitle());
  }

}
