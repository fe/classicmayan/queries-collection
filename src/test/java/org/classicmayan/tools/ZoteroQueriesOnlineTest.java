/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * FIXME: Check tests with valid Zotero API key!
 */
@Ignore
public class ZoteroQueriesOnlineTest {

  public static ZoteroQueries zotQue = new ZoteroQueries();

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUp() throws Exception {

    try (InputStream input = new FileInputStream("src/main/idiomQueries.properties")) {
      Properties prop = new Properties();
      prop.load(input);

      zotQue.setApiKey(prop.getProperty("zotero.apiKey"));

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void getResponseFromZotero() {
    ZoteroQueries zotQue = new ZoteroQueries();

    try {
      zotQue.connectToZoteroDB();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void getJSONResponseFromZotero() throws JSONException, IOException {

    JSONArray json = ZoteroQueries.readJsonFromUrl(
        "https://api.zotero.org/users/1757564/items?sort=date&limit=40&format=json&key="
            + zotQue.getApiKey() + "&q=Tikal");

    for (int i = 0; i < json.length(); i++) {
      JSONObject jObject = json.getJSONObject(i);
      System.out.println(jObject.getJSONObject("data").getString("itemType"));
    }
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void getCitationsForEachItem() throws JSONException, IOException {
    zotQue.getCitations();
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void getCitationsByKey() throws JSONException, IOException {
    System.out.println(zotQue.getCitationByKey("NJR5FBET"));
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void getAuthorsOfObject() throws JSONException, IOException {
    JSONArray jsonArray = ZoteroQueries
        .readJsonFromUrl("https://api.zotero.org/users/1757564/items?itemKey=XM9RVCNV");

    System.out.println(zotQue.extractAuthors(jsonArray.getJSONObject(0)));
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void getEditorsOfObject() throws JSONException, IOException {
    JSONArray jsonArray = ZoteroQueries
        .readJsonFromUrl("https://api.zotero.org/users/1757564/items?itemKey=XM9RVCNV");
    System.out.println(zotQue.extractEditors(jsonArray.getJSONObject(0)));
  }

}
