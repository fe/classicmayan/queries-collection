/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.classicmayan.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
*
*/
@Ignore
public class OrientDBQueriesOnlineTest {

  static OrientDBQueries odbq = new OrientDBQueries();

  public static String json = "{\n" +
      "    \"account\": \"#45:0\",\n" +
      "    \"meta\": {\n" +
      "        \"rid:\" : \"textgrid:5230.0\",\n" +
      "        \"created\": \"2021-10-03T21:10:21.77-07:00\",\n" +
      "        \"ip\": \"0:0:0:0:0:0:0:1\",\n" +
      "        \"contentType\": \"application/x-www-form-urlencoded\",\n" +
      "        \"userAgent\": \"PostmanRuntime/2.5.2\"\n" +
      "    },\n" +
      "    \"data\": \"hatschi\"\n" +
      "}";

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUp() throws Exception {

    try (InputStream input = new FileInputStream("src/main/resources/idiomQueries.properties")) {
      Properties prop = new Properties();
      prop.load(input);
      odbq.setOrientDBAccess(prop.getProperty("odb.password"));

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * 
   */
  /**
   * 
   */
  @Test
  @Ignore
  public void createNewDocumentFromJsonString() {
    odbq.createNewDocument(json);
  }

  /**
   * 
   */
  @Test
  public void readFromOrientDB() {
    System.out.println(odbq.readDocument("#57:0").toJSON());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void updateDocument() {
    odbq.updateDocument("63:0");
  }

}
