## [1.6.1](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/compare/v1.6.0...v1.6.1) (2023-10-11)


### Bug Fixes

* add java 1.8 and compiler plugin version to pom file, add error mesage if sid is invalid ([78d62d8](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/78d62d852a1f8c62af16055bc304f97248a2ed31))

# [1.6.0](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/compare/v1.5.0...v1.6.0) (2023-02-28)


### Bug Fixes

* add doc to all prop files ([dc20d29](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/dc20d2916d00763a4e41e56559b085d2836497a8))
* add logging ([7f93d52](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/7f93d525d29c57e0ed894aba6535eef6f022efce))
* add overall update-and-import-new method ([dc91e60](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/dc91e6095d4c217cef2a45e54f8b15802a3dd48a))


### Features

* prepare automation of update and import new image metadata ([a4fe9f3](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/a4fe9f30bafc5f3ac67b1c9d45bc2f3deddca4bf))

# [1.5.0](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/compare/v1.4.0...v1.5.0) (2023-02-10)


### Bug Fixes

* add new method for importing missing objects, update doc ([e209ca5](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/e209ca5fb00c18019f1b67b7a7a624a2ac75a525))


### Features

* remove deprecated method for importing image metadata to tg ([6cce130](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/6cce13092ef0a2977838024a1da3ceb9c2968338))

# [1.4.0](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/compare/v1.3.1...v1.4.0) (2023-02-09)


### Features

* add new method for checking KOR IDs with TextGrid IDs ([0608eed](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/0608eed350e6f940c5567174b236d6833305ff8c))

## [1.3.1](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/compare/v1.3.0...v1.3.1) (2022-12-02)


### Bug Fixes

* add title checks on update ([40b7c12](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/40b7c12688d11bcc8f3883e1e05117df41dc9f69))
* fix some small issues for first update and import of new files ([7a39911](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/7a39911fee0af2d13b9091cbadfaec1bb7c434b7))
* improve update ([0397cf6](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/0397cf6d6fb0c500265338409e160bcde6caf988))

# [1.3.0](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/compare/v1.2.14...v1.3.0) (2022-11-25)


### Bug Fixes

* add git ignore file for secret config files ([68ddb01](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/68ddb01568a425c60088af38d4fdfb7f8bec5a5f))
* add more ConedaKOR queries and tgsearch concerned methods ([483032c](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/483032ca31c700b64221c6831edbabb76bbe3dad))
* add new release of tg java clients version ([d97bd00](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/d97bd00a5e665881d139d61eba6ea52f001eb9f0))
* add new test for update/import of modified and new image metadata ([52ba780](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/52ba780914ad79a7cc14c430cbe459a4671c8b29))
* add some more endpoints, sort things ([cc40111](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/cc40111dd444e09de541419a05dd954966914f6f))
* finish importing new objects ([77abe4f](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/77abe4f1e4e3b1909b4aa98dc311b1b31a20d175))
* refactor saveToTextgrid method ([1a161fd](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/1a161fd529e9d922c0e87614413f4f05cf984709))


### Features

* add import for all images from conedakor ([acd1a89](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/acd1a89ac3769b43c499f1b5b58a20d9ee057cf2))
* add update, importNew, and importAll features ([413cbe4](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/413cbe4852579114a7b1dd5fe72535ef110ed6ed))

## [1.2.14](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/compare/v1.2.13...v1.2.14) (2022-11-09)


### Bug Fixes

* aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahhhhhhhhhhhhhhhhhhHHH!!!!!!!!!!!!!!!!!!! ([a0963aa](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/a0963aa414bb3776adea613a14f98180342e3f4e))
* add different template declaration ([42cf31f](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/42cf31fc2577065d1221ae906892cc18745624d3))
* add dry-run trigger ([3fab349](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/3fab3490d5b9724ac785a262ad44dde4ba8c12a2))
* add tempaltes again ([7348bab](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/7348baba2f9224cb0b73378e68fe193ed670bb55))
* mc ([52cd34e](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/52cd34e636817ec105ec0d5fa9e0359a901ca459))
* next try ([3597c1c](https://gitlab.gwdg.de/fe/classicmayan/queries-collection/commit/3597c1c9da6908464467a8c6ff49755a769e9a8b))

# 1.2.8

* Add new query to get EPPN from SID
* Add README.md
* Add license file
* Add new template for pom version
